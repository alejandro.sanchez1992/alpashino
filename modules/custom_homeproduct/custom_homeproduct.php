<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

$autoloadPath = __DIR__ . '/vendor/autoload.php';
if (file_exists($autoloadPath)) {
    require_once $autoloadPath;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Custom_Homeproduct extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'custom_homeproduct';
        $this->author = 'Alejandro';
        $this->version = '1.0.0';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('My module home product');
        $this->description = $this->l('Home categories.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:'.$this->name.'/views/templates/hook/custom_homeproduct.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('top')
            && $this->registerHook('displayHome')
            && $this->registerHook('CustomHomeProduct')
            && $this->registerHook('header')
        ;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('custom_homeproduct')
        ) {
            return false;
        }
    
        return true;
    }

    public function hookHeader()
    {   
        $this->context->controller->registerStylesheet('modules-custom_homeproduct', 'modules/'.$this->name.'/views/css/custom_homeproduct.css', ['media' => 'all', 'priority' => 150]);
    }


    public function getWidgetVariables($hookName, array $configuration = [])
    {
        $widgetVariables = array(
            'categories' => Category::getChildren(2, 2),
        );

        return $widgetVariables;
    }

    public function renderWidget($hookName, array $configuration = [])
    {
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

        return $this->fetch($this->templateFile);
    }
}
