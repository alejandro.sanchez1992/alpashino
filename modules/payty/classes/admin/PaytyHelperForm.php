<?php
/**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

if (! defined('_PS_VERSION_')) {
    exit;
}

/**
 * Class that renders payment module administration interface.
 */
class PaytyHelperForm
{
    private function __construct()
    {
        // Do not instantiate this class.
    }

    public static function getAdminFormContext()
    {
        $context = Context::getContext();

        /* @var Payty */
        $payty = Module::getInstanceByName('payty');

        $languages = array();
        foreach (PaytyApi::getSupportedLanguages() as $code => $label) {
            $languages[$code] = $payty->l($label, 'paytyhelperform');
        }

        asort($languages);

        $category_options = array(
            'FOOD_AND_GROCERY' => $payty->l('Food and grocery', 'paytyhelperform'),
            'AUTOMOTIVE' => $payty->l('Automotive', 'paytyhelperform'),
            'ENTERTAINMENT' => $payty->l('Entertainment', 'paytyhelperform'),
            'HOME_AND_GARDEN' => $payty->l('Home and garden', 'paytyhelperform'),
            'HOME_APPLIANCE' => $payty->l('Home appliance', 'paytyhelperform'),
            'AUCTION_AND_GROUP_BUYING' => $payty->l('Auction and group buying', 'paytyhelperform'),
            'FLOWERS_AND_GIFTS' => $payty->l('Flowers and gifts', 'paytyhelperform'),
            'COMPUTER_AND_SOFTWARE' => $payty->l('Computer and software', 'paytyhelperform'),
            'HEALTH_AND_BEAUTY' => $payty->l('Health and beauty', 'paytyhelperform'),
            'SERVICE_FOR_INDIVIDUAL' => $payty->l('Service for individual', 'paytyhelperform'),
            'SERVICE_FOR_BUSINESS' => $payty->l('Service for business', 'paytyhelperform'),
            'SPORTS' => $payty->l('Sports', 'paytyhelperform'),
            'CLOTHING_AND_ACCESSORIES' => $payty->l('Clothing and accessories', 'paytyhelperform'),
            'TRAVEL' => $payty->l('Travel', 'paytyhelperform'),
            'HOME_AUDIO_PHOTO_VIDEO' => $payty->l('Home audio, photo, video', 'paytyhelperform'),
            'TELEPHONY' => $payty->l('Telephony', 'paytyhelperform')
        );

        // Get documentation links.
        $doc_files = array();
        $filenames = glob(_PS_MODULE_DIR_ . 'payty/installation_doc/' . PaytyTools::getDocPattern());

        $doc_languages = array(
            'fr' => 'Français',
            'en' => 'English',
            'es' => 'Español',
            'de' => 'Deutsch'
            // Complete when other languages are managed.
        );

        foreach ($filenames as $filename) {
            $base_filename = basename($filename, '.pdf');
            $lang = Tools::substr($base_filename, -2); // Extract language code.

            $doc_files[$base_filename . '.pdf'] = $doc_languages[$lang];
        }

        $placeholders = self::getArrayConfig('PAYTY_STD_REST_PLACEHLDR');
        if (empty($placeholders)) {
            $placeholders = array('pan' => '', 'expiry' => '', 'cvv' => '');
        }

        $enabledCountries = Country::getCountries((int) $context->language->id, true);
        $all_countries = Country::getCountries((int) $context->language->id, false);
        $countryList = array();
        foreach ($enabledCountries as $value) {
            $countryList['ps_countries'][$value['iso_code']] = $value['name'];
        }

        foreach (PaytyTools::$submodules as $key => $module) {
            $module_class_name = 'Payty' . $module.'Payment';
            $instance_module = new $module_class_name();
            if (method_exists($instance_module, 'getCountries') && $instance_module->getCountries()) {
                $submodule_specific_countries = $instance_module->getCountries();
                foreach ($submodule_specific_countries as $country) {
                    if (isset($countryList['ps_countries'][$country])) {
                        $countryList[$key][$country] = $countryList['ps_countries'][$country];
                    }
                }
            }
        }

        foreach ($all_countries as $value) {
            if ($value['iso_code'] === 'FR') {
                $countryList['FULLCB']['FR'] = $value['name'];
                break;
            }
        }

        $tpl_vars = array(
            'payty_support_email' => PaytyTools::getDefault('SUPPORT_EMAIL'),
            'payty_plugin_version' => PaytyTools::getDefault('PLUGIN_VERSION'),
            'payty_gateway_version' => PaytyTools::getDefault('GATEWAY_VERSION'),
            'payty_contrib' => PaytyTools::getContrib(),
            'payty_installed_modules' => PaytyTools::getModulesInstalled(),
            'payty_card_data_entry_modes' => PaytyTools::getCardDataEntryModes(),
            'payty_employee' => $context->employee,

            'payty_plugin_features' => PaytyTools::$plugin_features,
            'payty_request_uri' => $_SERVER['REQUEST_URI'],

            'payty_doc_files' => $doc_files,
            'payty_enable_disable_options' => array(
                'False' => $payty->l('Disabled', 'paytyhelperform'),
                'True' => $payty->l('Enabled', 'paytyhelperform')
            ),
            'payty_mode_options' => array(
                'TEST' => $payty->l('TEST', 'paytyhelperform'),
                'PRODUCTION' => $payty->l('PRODUCTION', 'paytyhelperform')
            ),
            'payty_language_options' => $languages,
            'payty_validation_mode_options' => array(
                '' => $payty->l('Bank Back Office configuration', 'paytyhelperform'),
                '0' => $payty->l('Automatic', 'paytyhelperform'),
                '1' => $payty->l('Manual', 'paytyhelperform')
            ),
            'payty_payment_cards_options' => array('' => $payty->l('ALL', 'paytyhelperform')) + PaytyTools::getSupportedCardTypes(),
            'payty_multi_payment_cards_options' => array('' => $payty->l('ALL', 'paytyhelperform')) + PaytyTools::getSupportedMultiCardTypes(),
            'payty_category_options' => $category_options,
            'payty_yes_no_options' => array(
                'False' => $payty->l('No', 'paytyhelperform'),
                'True' => $payty->l('Yes', 'paytyhelperform')
            ),
            'payty_delivery_type_options' => array(
                'PACKAGE_DELIVERY_COMPANY' => $payty->l('Delivery company', 'paytyhelperform'),
                'RECLAIM_IN_SHOP' => $payty->l('Reclaim in shop', 'paytyhelperform'),
                'RELAY_POINT' => $payty->l('Relay point', 'paytyhelperform'),
                'RECLAIM_IN_STATION' => $payty->l('Reclaim in station', 'paytyhelperform')
            ),
            'payty_delivery_speed_options' => array(
                'STANDARD' => $payty->l('Standard', 'paytyhelperform'),
                'EXPRESS' => $payty->l('Express', 'paytyhelperform'),
                'PRIORITY' => $payty->l('Priority', 'paytyhelperform')
            ),
            'payty_delivery_delay_options' => array(
                'INFERIOR_EQUALS' => $payty->l('<= 1 hour', 'paytyhelperform'),
                'SUPERIOR' => $payty->l('> 1 hour', 'paytyhelperform'),
                'IMMEDIATE' => $payty->l('Immediate', 'paytyhelperform'),
                'ALWAYS' => $payty->l('24/7', 'paytyhelperform')
            ),
            'payty_failure_management_options' => array(
                PaytyTools::ON_FAILURE_RETRY => $payty->l('Go back to checkout', 'paytyhelperform'),
                PaytyTools::ON_FAILURE_SAVE => $payty->l('Save order and go back to order history', 'paytyhelperform')
            ),
            'payty_cart_management_options' => array(
                PaytyTools::EMPTY_CART => $payty->l('Empty cart to avoid amount errors', 'paytyhelperform'),
                PaytyTools::KEEP_CART => $payty->l('Keep cart (PrestaShop default behavior)', 'paytyhelperform')
            ),
            'payty_card_data_mode_options' => array(
                '1' => $payty->l('Bank data acquisition on payment gateway', 'paytyhelperform'),
                '2' => $payty->l('Card type selection on merchant site', 'paytyhelperform'),
                '4' => $payty->l('Payment page integrated to checkout process (iframe mode)', 'paytyhelperform'),
                '5' => $payty->l('Embedded payment fields on merchant site (REST API)', 'paytyhelperform'),
                '6' => $payty->l('Embedded payment fields in a pop-in (REST API)', 'paytyhelperform')
            ),
            'payty_countries_options' => array(
                '1' => $payty->l('All Allowed Countries', 'paytyhelperform'),
                '2' => $payty->l('Specific Countries', 'paytyhelperform')
            ),
            'payty_countries_list' => $countryList,
            'payty_card_selection_mode_options' => array(
                '1' => $payty->l('On payment gateway', 'paytyhelperform'),
                '2' => $payty->l('On merchant site', 'paytyhelperform')
            ),
            'payty_default_multi_option' => array(
                'label' => '',
                'min_amount' => '',
                'max_amount' => '',
                'contract' => '',
                'count' => '',
                'period' => '',
                'first' => ''
            ),
            'payty_default_oney_option' => array(
                'label' => '',
                'code' => '',
                'min_amount' => '',
                'max_amount' => '',
                'count' => '',
                'rate' => ''
            ),
            'payty_default_franfinance_option' => array(
                'label' => '',
                'count' => '3',
                'fees' => '-1',
                'min_amount' => '100',
                'max_amount' => '3000'
            ),
            'franfinance_count' => array(
                '3' => '3x',
                '4' => '4x'
            ),
            'fees_options' => array(
                '-1' => $payty->l('Bank Back Office configuration', 'paytyhelperform'),
                '0' => $payty->l('Without fees', 'paytyhelperform'),
                '1' => $payty->l('With fees', 'paytyhelperform')
            ),
            'payty_default_other_payment_means_option' => array(
                'title' => '',
                'code' => '',
                'min_amount' => '',
                'max_amount' => '',
                'validation' => '-1',
                'capture' => '',
                'cart' => 'False'
            ),
            'payty_default_extra_payment_means_option' => array(
                'code' => '',
                'title' => ''
            ),
            'payty_std_rest_theme_options' => array(
                'classic' =>  'Classic',
                'material' => 'Material'
            ),

            'prestashop_categories' => Category::getCategories((int) $context->language->id, true, false),
            'prestashop_languages' => Language::getLanguages(false),
            'prestashop_lang' => Language::getLanguage((int) $context->language->id),
            'prestashop_carriers' => Carrier::getCarriers(
                (int) $context->language->id,
                true,
                false,
                false,
                null,
                Carrier::ALL_CARRIERS
            ),
            'prestashop_groups' => self::getAuthorizedGroups(),
            'payty_sepa_mandate_mode_options' => array(
                'PAYMENT' => $payty->l('One-off SEPA direct debit', 'paytyhelperform'),
                'REGISTER_PAY' => $payty->l('Register a recurrent SEPA mandate with direct debit', 'paytyhelperform'),
                'REGISTER' => $payty->l('Register a recurrent SEPA mandate without direct debit', 'paytyhelperform')
            ),

            'PAYTY_ENABLE_LOGS' => Configuration::get('PAYTY_ENABLE_LOGS'),

            'PAYTY_SITE_ID' => Configuration::get('PAYTY_SITE_ID'),
            'PAYTY_KEY_TEST' => Configuration::get('PAYTY_KEY_TEST'),
            'PAYTY_KEY_PROD' => Configuration::get('PAYTY_KEY_PROD'),
            'PAYTY_MODE' => Configuration::get('PAYTY_MODE'),
            'PAYTY_SIGN_ALGO' => Configuration::get('PAYTY_SIGN_ALGO'),
            'PAYTY_PLATFORM_URL' => Configuration::get('PAYTY_PLATFORM_URL'),
            'PAYTY_NOTIFY_URL' => self::getIpnUrl(),

            'PAYTY_PUBKEY_TEST' => Configuration::get('PAYTY_PUBKEY_TEST'),
            'PAYTY_PRIVKEY_TEST' => Configuration::get('PAYTY_PRIVKEY_TEST'),
            'PAYTY_ENABLE_WS' => Configuration::get('PAYTY_ENABLE_WS'),
            'PAYTY_PUBKEY_PROD' => Configuration::get('PAYTY_PUBKEY_PROD'),
            'PAYTY_PRIVKEY_PROD' => Configuration::get('PAYTY_PRIVKEY_PROD'),
            'PAYTY_RETKEY_TEST' => Configuration::get('PAYTY_RETKEY_TEST'),
            'PAYTY_RETKEY_PROD' => Configuration::get('PAYTY_RETKEY_PROD'),
            'PAYTY_REST_NOTIFY_URL' => self::getIpnUrl(),
            'PAYTY_REST_SERVER_URL' => Configuration::get('PAYTY_REST_SERVER_URL'),
            'PAYTY_REST_JS_CLIENT_URL' => Configuration::get('PAYTY_REST_JS_CLIENT_URL'),

            'PAYTY_DEFAULT_LANGUAGE' => Configuration::get('PAYTY_DEFAULT_LANGUAGE'),
            'PAYTY_AVAILABLE_LANGUAGES' => ! Configuration::get('PAYTY_AVAILABLE_LANGUAGES') ?
                                            array('') :
                                            explode(';', Configuration::get('PAYTY_AVAILABLE_LANGUAGES')),
            'PAYTY_DELAY' => Configuration::get('PAYTY_DELAY'),
            'PAYTY_VALIDATION_MODE' => Configuration::get('PAYTY_VALIDATION_MODE'),

            'PAYTY_THEME_CONFIG' => self::getLangConfig('PAYTY_THEME_CONFIG'),
            'PAYTY_SHOP_NAME' => Configuration::get('PAYTY_SHOP_NAME'),
            'PAYTY_SHOP_URL' => Configuration::get('PAYTY_SHOP_URL'),

            'PAYTY_3DS_MIN_AMOUNT' => self::getArrayConfig('PAYTY_3DS_MIN_AMOUNT'),

            'PAYTY_REDIRECT_ENABLED' => Configuration::get('PAYTY_REDIRECT_ENABLED'),
            'PAYTY_REDIRECT_SUCCESS_T' => Configuration::get('PAYTY_REDIRECT_SUCCESS_T'),
            'PAYTY_REDIRECT_SUCCESS_M' => self::getLangConfig('PAYTY_REDIRECT_SUCCESS_M'),
            'PAYTY_REDIRECT_ERROR_T' => Configuration::get('PAYTY_REDIRECT_ERROR_T'),
            'PAYTY_REDIRECT_ERROR_M' => self::getLangConfig('PAYTY_REDIRECT_ERROR_M'),
            'PAYTY_RETURN_MODE' => Configuration::get('PAYTY_RETURN_MODE'),
            'PAYTY_FAILURE_MANAGEMENT' => Configuration::get('PAYTY_FAILURE_MANAGEMENT'),
            'PAYTY_CART_MANAGEMENT' => Configuration::get('PAYTY_CART_MANAGEMENT'),

            'PAYTY_SEND_CART_DETAIL' => Configuration::get('PAYTY_SEND_CART_DETAIL'),
            'PAYTY_COMMON_CATEGORY' => Configuration::get('PAYTY_COMMON_CATEGORY'),
            'PAYTY_CATEGORY_MAPPING' => self::getArrayConfig('PAYTY_CATEGORY_MAPPING'),
            'PAYTY_SEND_SHIP_DATA' => Configuration::get('PAYTY_SEND_SHIP_DATA'),
            'PAYTY_ONEY_SHIP_OPTIONS' => self::getArrayConfig('PAYTY_ONEY_SHIP_OPTIONS'),

            'PAYTY_STD_TITLE' => self::getLangConfig('PAYTY_STD_TITLE'),
            'PAYTY_STD_ENABLED' => Configuration::get('PAYTY_STD_ENABLED'),
            'PAYTY_STD_AMOUNTS' => self::getArrayConfig('PAYTY_STD_AMOUNTS'),
            'PAYTY_STD_DELAY' => Configuration::get('PAYTY_STD_DELAY'),
            'PAYTY_STD_VALIDATION' => Configuration::get('PAYTY_STD_VALIDATION'),
            'PAYTY_STD_PAYMENT_CARDS' => ! Configuration::get('PAYTY_STD_PAYMENT_CARDS') ?
                                            array('') :
                                            explode(';', Configuration::get('PAYTY_STD_PAYMENT_CARDS')),
            'PAYTY_STD_PROPOSE_ONEY' => Configuration::get('PAYTY_STD_PROPOSE_ONEY'),
            'PAYTY_STD_CARD_DATA_MODE' => Configuration::get('PAYTY_STD_CARD_DATA_MODE'),
            'PAYTY_STD_REST_THEME' => Configuration::get('PAYTY_STD_REST_THEME'),
            'PAYTY_STD_REST_PLACEHLDR' => $placeholders,
            'PAYTY_STD_REST_LBL_REGIST' => self::getLangConfig('PAYTY_STD_REST_LBL_REGIST'),
            'PAYTY_STD_REST_ATTEMPTS' => Configuration::get('PAYTY_STD_REST_ATTEMPTS'),
            'PAYTY_STD_1_CLICK_PAYMENT' => Configuration::get('PAYTY_STD_1_CLICK_PAYMENT'),
            'PAYTY_STD_CANCEL_IFRAME' => Configuration::get('PAYTY_STD_CANCEL_IFRAME'),

            'PAYTY_MULTI_TITLE' => self::getLangConfig('PAYTY_MULTI_TITLE'),
            'PAYTY_MULTI_ENABLED' => Configuration::get('PAYTY_MULTI_ENABLED'),
            'PAYTY_MULTI_AMOUNTS' => self::getArrayConfig('PAYTY_MULTI_AMOUNTS'),
            'PAYTY_MULTI_DELAY' => Configuration::get('PAYTY_MULTI_DELAY'),
            'PAYTY_MULTI_VALIDATION' => Configuration::get('PAYTY_MULTI_VALIDATION'),
            'PAYTY_MULTI_CARD_MODE' => Configuration::get('PAYTY_MULTI_CARD_MODE'),
            'PAYTY_MULTI_PAYMENT_CARDS' => ! Configuration::get('PAYTY_MULTI_PAYMENT_CARDS') ?
                                            array('') :
                                            explode(';', Configuration::get('PAYTY_MULTI_PAYMENT_CARDS')),
            'PAYTY_MULTI_OPTIONS' => self::getArrayConfig('PAYTY_MULTI_OPTIONS'),

            'PAYTY_ANCV_TITLE' => self::getLangConfig('PAYTY_ANCV_TITLE'),
            'PAYTY_ANCV_ENABLED' => Configuration::get('PAYTY_ANCV_ENABLED'),
            'PAYTY_ANCV_AMOUNTS' => self::getArrayConfig('PAYTY_ANCV_AMOUNTS'),
            'PAYTY_ANCV_DELAY' => Configuration::get('PAYTY_ANCV_DELAY'),
            'PAYTY_ANCV_VALIDATION' => Configuration::get('PAYTY_ANCV_VALIDATION'),

            'PAYTY_ONEY_TITLE' => self::getLangConfig('PAYTY_ONEY_TITLE'),
            'PAYTY_ONEY_ENABLED' => Configuration::get('PAYTY_ONEY_ENABLED'),
            'PAYTY_ONEY_AMOUNTS' => self::getArrayConfig('PAYTY_ONEY_AMOUNTS'),
            'PAYTY_ONEY_DELAY' => Configuration::get('PAYTY_ONEY_DELAY'),
            'PAYTY_ONEY_VALIDATION' => Configuration::get('PAYTY_ONEY_VALIDATION'),
            'PAYTY_ONEY_ENABLE_OPTIONS' => Configuration::get('PAYTY_ONEY_ENABLE_OPTIONS'),
            'PAYTY_ONEY_OPTIONS' => self::getArrayConfig('PAYTY_ONEY_OPTIONS'),

            'PAYTY_ONEY34_TITLE' => self::getLangConfig('PAYTY_ONEY34_TITLE'),
            'PAYTY_ONEY34_ENABLED' => Configuration::get('PAYTY_ONEY34_ENABLED'),
            'PAYTY_ONEY34_AMOUNTS' => self::getArrayConfig('PAYTY_ONEY34_AMOUNTS'),
            'PAYTY_ONEY34_DELAY' => Configuration::get('PAYTY_ONEY34_DELAY'),
            'PAYTY_ONEY34_VALIDATION' => Configuration::get('PAYTY_ONEY34_VALIDATION'),
            'PAYTY_ONEY34_OPTIONS' => self::getArrayConfig('PAYTY_ONEY34_OPTIONS'),

            'PAYTY_FFIN_TITLE' => self::getLangConfig('PAYTY_FFIN_TITLE'),
            'PAYTY_FFIN_ENABLED' => Configuration::get('PAYTY_FFIN_ENABLED'),
            'PAYTY_FFIN_AMOUNTS' => self::getArrayConfig('PAYTY_FFIN_AMOUNTS'),
            'PAYTY_FFIN_OPTIONS' => self::getArrayConfig('PAYTY_FFIN_OPTIONS'),

            'PAYTY_FULLCB_TITLE' => self::getLangConfig('PAYTY_FULLCB_TITLE'),
            'PAYTY_FULLCB_ENABLED' => Configuration::get('PAYTY_FULLCB_ENABLED'),
            'PAYTY_FULLCB_AMOUNTS' => self::getArrayConfig('PAYTY_FULLCB_AMOUNTS'),
            'PAYTY_FULLCB_ENABLE_OPTS' => Configuration::get('PAYTY_FULLCB_ENABLE_OPTS'),
            'PAYTY_FULLCB_OPTIONS' => self::getArrayConfig('PAYTY_FULLCB_OPTIONS'),

            'PAYTY_SEPA_TITLE' => self::getLangConfig('PAYTY_SEPA_TITLE'),
            'PAYTY_SEPA_ENABLED' => Configuration::get('PAYTY_SEPA_ENABLED'),
            'PAYTY_SEPA_AMOUNTS' => self::getArrayConfig('PAYTY_SEPA_AMOUNTS'),
            'PAYTY_SEPA_DELAY' => Configuration::get('PAYTY_SEPA_DELAY'),
            'PAYTY_SEPA_VALIDATION' => Configuration::get('PAYTY_SEPA_VALIDATION'),
            'PAYTY_SEPA_MANDATE_MODE' => Configuration::get('PAYTY_SEPA_MANDATE_MODE'),
            'PAYTY_SEPA_1_CLICK_PAYMNT' => Configuration::get('PAYTY_SEPA_1_CLICK_PAYMNT'),

            'PAYTY_SOFORT_TITLE' => self::getLangConfig('PAYTY_SOFORT_TITLE'),
            'PAYTY_SOFORT_ENABLED' => Configuration::get('PAYTY_SOFORT_ENABLED'),
            'PAYTY_SOFORT_AMOUNTS' => self::getArrayConfig('PAYTY_SOFORT_AMOUNTS'),

            'PAYTY_PAYPAL_TITLE' => self::getLangConfig('PAYTY_PAYPAL_TITLE'),
            'PAYTY_PAYPAL_ENABLED' => Configuration::get('PAYTY_PAYPAL_ENABLED'),
            'PAYTY_PAYPAL_AMOUNTS' => self::getArrayConfig('PAYTY_PAYPAL_AMOUNTS'),
            'PAYTY_PAYPAL_DELAY' => Configuration::get('PAYTY_PAYPAL_DELAY'),
            'PAYTY_PAYPAL_VALIDATION' => Configuration::get('PAYTY_PAYPAL_VALIDATION'),

            'PAYTY_CHOOZEO_TITLE' => self::getLangConfig('PAYTY_CHOOZEO_TITLE'),
            'PAYTY_CHOOZEO_ENABLED' => Configuration::get('PAYTY_CHOOZEO_ENABLED'),
            'PAYTY_CHOOZEO_AMOUNTS' => self::getArrayConfig('PAYTY_CHOOZEO_AMOUNTS'),
            'PAYTY_CHOOZEO_DELAY' => Configuration::get('PAYTY_CHOOZEO_DELAY'),
            'PAYTY_CHOOZEO_OPTIONS' => self::getArrayConfig('PAYTY_CHOOZEO_OPTIONS'),

            'PAYTY_OTHER_GROUPED_VIEW' => Configuration::get('PAYTY_OTHER_GROUPED_VIEW'),
            'PAYTY_OTHER_ENABLED' => Configuration::get('PAYTY_OTHER_ENABLED'),
            'PAYTY_OTHER_TITLE' => self::getLangConfig('PAYTY_OTHER_TITLE'),
            'PAYTY_OTHER_AMOUNTS' => self::getArrayConfig('PAYTY_OTHER_AMOUNTS'),
            'PAYTY_OTHER_PAYMENT_MEANS' => self::getArrayConfig('PAYTY_OTHER_PAYMENT_MEANS'),
            'PAYTY_EXTRA_PAYMENT_MEANS' => self::getArrayConfig('PAYTY_EXTRA_PAYMENT_MEANS')
        );

        foreach (PaytyTools::$submodules as $key => $module) {
            $tpl_vars['PAYTY_' . $key . '_COUNTRY'] = Configuration::get('PAYTY_' . $key . '_COUNTRY');
            $tpl_vars['PAYTY_' . $key . '_COUNTRY_LST'] = ! Configuration::get('PAYTY_' . $key . '_COUNTRY_LST') ?
                array() : explode(';', Configuration::get('PAYTY_' . $key . '_COUNTRY_LST'));
        }

        if (! PaytyTools::$plugin_features['embedded']) {
            unset($tpl_vars['payty_card_data_mode_options'][PaytyTools::MODE_EMBEDDED]);
            unset($tpl_vars['payty_card_data_mode_options'][PaytyTools::MODE_POPIN]);
        }

        return $tpl_vars;
    }

    private static function getIpnUrl()
    {
        $shop = new Shop(Configuration::get('PS_SHOP_DEFAULT'));

        // SSL enabled on default shop?
        $id_shop_group = isset($shop->id_shop_group) ? $shop->id_shop_group : $shop->id_group_shop;
        $ssl = Configuration::get('PS_SSL_ENABLED', null, $id_shop_group, $shop->id);

        $ipn = ($ssl ? 'https://' . $shop->domain_ssl : 'http://' . $shop->domain)
            . $shop->getBaseURI() . 'modules/payty/validation.php';

        return $ipn;
    }

    private static function getArrayConfig($name)
    {
        $value = @unserialize(Configuration::get($name));

        if (! is_array($value)) {
            $value = array();
        }

        return $value;
    }

    private static function getLangConfig($name)
    {
        $languages = Language::getLanguages(false);

        $result = array();
        foreach ($languages as $language) {
            $result[$language['id_lang']] = Configuration::get($name, $language['id_lang']);
        }

        return $result;
    }

    private static function getAuthorizedGroups()
    {
        $context = Context::getContext();

        /* @var Payty */
        $payty = Module::getInstanceByName('payty');

        $sql = 'SELECT DISTINCT gl.`id_group`, gl.`name` FROM `' . _DB_PREFIX_ . 'group_lang` AS gl
            INNER JOIN `' . _DB_PREFIX_ . 'module_group` AS mg
            ON (
                gl.`id_group` = mg.`id_group`
                AND mg.`id_module` = ' . (int) $payty->id . '
                AND mg.`id_shop` = ' . (int) $context->shop->id . '
            )
            WHERE gl.`id_lang` = ' . (int) $context->language->id;

        return Db::getInstance()->executeS($sql);
    }
}
