1.14.1, 2021-04-01:
- Bug fix: Do not refund payments when vouchers are genereated in PrestShop 1.6.x.
- Bug fix: Do not save payments with negative amount in PrestaShop 1.7.7.x.
- Update 3DS management option description.
- Improve REST API keys configuration display.
- Possibility to disable web services for order operations in PrestaShop Back Office.

1.14.0, 2021-03-03:
- Bug fix: Update order status after multiple payment tries or on cancellation from gateway Back Office.
- [embedded] Add the pop-in choice to card data entry mode setting.
- [embedded] Possibility to customize the "Register my card" checkbox label for embedded payment mode.
- Possibility to configure REST API URLs.
- [alias] Check alias validity before proceeding to payment.
- Possibility to refund payments in installments.
- Possibility to refund/cancel payment online when the order is cancelled in PrestaShop Back Office.
- Possibility to add payment means dynamically in "Other payment means" section.
- Do not use vads_order_info\* gateway parameter (use vads_ext_info_\* instead).
- Possibility to open a support issue from the plugin configuration interface.
- Use the online payment means logos.
- Identify MOTO payments for orders from PrestaShop Back Office.
- Improve installation process (do not stop installation if PrestaShop errors are thrown).
- Possibility to upgrade the module from the PrestaShop backend.
- [technical] Load plugin classes dynamically.

1.13.8, 2020-12-10:
- Bug fix: Incorrectly formatted amount in order confirmation page.
- Bug fix: Error 500 due to obsolete function (get_magic_quotes_gpc) in PHP 7.4.
- Consider case of chargedbacks when refunding.
- Display warning message on payment in iframe mode enabling.

1.13.7, 2020-11-24:
- [embedded] Bug fix: Embedded payment fields not correctly displayed since the last gateway JS library delivery on PrestaShop 1.6.
- [embedded] Bug fix: Update token on minicart change on PrestaShop 1.6.
- Minor fix.

1.13.6, 2020-10-27:
- [embedded] Bug fix: Display 3DS result for REST API payments.
- Display warning message when only offline refund is possible.

1.13.5, 2020-10-05:
- Bug fix: Fix IPN management in multistore environment.
- Bug fix: Fix Order->total_real_paid value on payment cancellation.
- Bug fix: Possibility to refund orders offline if merchant did not configure REST API keys.

1.13.4, 2020-08-18:
- [embedded] Bug fix: Error due to strongAuthenticationState field renaming in REST token creation.
- [embedded] Minor code improve: use KR.openPopin() and KR.submit().
- [embedded] Improve payment with embedded fields button display in PrestaShop 1.6.x versions.
- Update payment means logos.

1.13.3, 2020-06-19:
- [embedded] Bug fix: Compatibility of payment with embedded fields with Internet Explorer 11.
- Bug fix: Possibility to make refunds for a payment with many attempts.
- [embedded] Bug fix: Fix JS error if payment token not created.
- Bug fix: Delete double invoice entry in ps_order_invoice_payment table.
- Improve refund payments feature.

1.13.2, 2020-05-20:
- [embedded] Manage new metadata field format returned in REST API IPN.
- Bug fix: Fix sent data according to new Transaction/Update REST WS.
- Send PrestaShop username and IP as a comment on refund WS calls.
- Improve some plugin translations.
- Improve redirection to gateway page.

1.13.1, 2020-04-07:
- Restore compatibility with PHP v5.3.
- [embedded] Bug fix: Payment fields error relative to new JavaScript client library.

1.13.0, 2020-03-04:
- Bug fix: Fix amount issue relative to multiple partial refunds.
- Bug fix: Shipping costs not included in the refunded amount through the PrestaShop backend.
- Improve payment statuses management.

1.12.1, 2020-02-04:
- [alias] Bug fix: card data was requested even if the buyer chose to use his registered means of payment.

1.12.0, 2020-01-30:
- Bug fix: 3DS result is not correctly saved in backend order details when using embedded payment fields.
- Bug fix: Fix theme config setting for iframe mode.
- [embedded] Added possibility to display REST API fields in pop-in mode.
- Possibility to make refunds for payments.
- Possibility to cancel payment in iframe mode.
- [alias] Added payment by token.
- [technical] Do not use vads\_order\_info2 gateway parameter.
- Removed feature data acquisition on merchant website.
- Possibility to not send shopping cart content when not mandatory.
- Restrict payment submodules to specific countries.