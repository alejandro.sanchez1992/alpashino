{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<script type="text/javascript">
  $(function() {
    $('#accordion').accordion({
      active: false,
      collapsible: true,
      autoHeight: false,
      heightStyle: 'content',
      header: 'h4',
      animated: false
    });

    $('contact-support').on('sendmail', function(e){
        $.ajax({
            type: "POST",
            url: "{$payty_request_uri}",
            data: e.originalEvent.detail,
            success: function(res) {
                location.reload();
            },
            dataType: 'html'
        });
    });
 });
</script>

<script type="text/javascript">
    function paytyCardEntryChanged() {
        var cardDataMode = $('select#PAYTY_STD_CARD_DATA_MODE option:selected').val();

        switch (cardDataMode) {
            case '4':
                if (! confirm('{l s='Warning, some payment means are not compatible with an integration by iframe. Please consult the documentation for more details.' mod='payty'}')) {
                    var oldCardDataMode = $('#PAYTY_STD_CARD_DATA_MODE_OLD').val();
                    $('select#PAYTY_STD_CARD_DATA_MODE').val(oldCardDataMode).change()
                } else {
                    $('#PAYTY_REST_SETTINGS').hide();
                    $('#PAYTY_STD_CANCEL_IFRAME_MENU').show();
                }

                break;
            case '5':
            case '6':
                $('#PAYTY_REST_SETTINGS').show();
                $('#PAYTY_STD_CANCEL_IFRAME_MENU').hide();
                break;
            default:
                $('#PAYTY_REST_SETTINGS').hide();
                $('#PAYTY_STD_CANCEL_IFRAME_MENU').hide();
        }
    }
</script>

<script type="text/javascript" src="../modules/payty/views/js/support.js"></script>

<form method="POST" action="{$payty_request_uri|escape:'html':'UTF-8'}" class="defaultForm form-horizontal">
  <div style="width: 100%;">
    <fieldset>
      <legend>
        <img style="width: 20px; vertical-align: middle;" src="../modules/payty/logo.png">Payty
      </legend>

      {l s='Developed by' mod='payty'} : <b>Lyra Network</b><br />
      {l s='Contact us' mod='payty'} : <b><a href="mailto:{$payty_support_email|escape:'html':'UTF-8'}">{$payty_support_email|escape:'html':'UTF-8'}</a></b><br />
      {l s='Module version' mod='payty'} : <b>{if $smarty.const._PS_HOST_MODE_|defined}Cloud{/if}{$payty_plugin_version|escape:'html':'UTF-8'}</b><br />
      {l s='Gateway version' mod='payty'} : <b>{$payty_gateway_version|escape:'html':'UTF-8'}</b><br />

      {if !empty($payty_doc_files)}
        <span style="color: red; font-weight: bold; text-transform: uppercase;">{l s='Click to view the module configuration documentation :' mod='payty'}</span>
        {foreach from=$payty_doc_files key="file" item="lang"}
          <a style="margin-left: 10px; font-weight: bold; text-transform: uppercase;" href="../modules/payty/installation_doc/{$file|escape:'html':'UTF-8'}" target="_blank">{$lang|escape:'html':'UTF-8'}</a>
        {/foreach}
        <br />
      {/if}

      <contact-support
          shop-id={$PAYTY_SITE_ID|escape:'html':'UTF-8'}
          context-mode={$PAYTY_MODE|escape:'html':'UTF-8'}
          sign-algo={$PAYTY_SIGN_ALGO|escape:'html':'UTF-8'}
          contrib={$payty_contrib|escape:'html':'UTF-8'}
          integration-mode={$payty_card_data_entry_modes[$PAYTY_STD_CARD_DATA_MODE]|escape:'html':'UTF-8'}
          plugins="{$payty_installed_modules|escape:'html':'UTF-8'}"
          title=""
          first-name="{$payty_employee->firstname|escape:'html':'UTF-8'}"
          last-name="{$payty_employee->lastname|escape:'html':'UTF-8'}"
          from-email="{$payty_employee->email|escape:'html':'UTF-8'}"
          to-email="{$payty_support_email|escape:'html':'UTF-8'}"
          cc-emails=""
          phone-number=""
          language="{$prestashop_lang.iso_code|escape:'html':'UTF-8'}"
      ></contact-support><br />
    </fieldset>
  </div>

  <br /><br />

  <div id="accordion" style="width: 100%; float: none;">
    <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
      <a href="#">{l s='GENERAL CONFIGURATION' mod='payty'}</a>
    </h4>
    <div>
      <fieldset>
        <legend>{l s='BASE SETTINGS' mod='payty'}</legend>

        <label for="PAYTY_ENABLE_LOGS">{l s='Logs' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_ENABLE_LOGS" name="PAYTY_ENABLE_LOGS">
            {foreach from=$payty_enable_disable_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ENABLE_LOGS === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='Enable / disbale module logs' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='PAYMENT GATEWAY ACCESS' mod='payty'}</legend>

        <label for="PAYTY_SITE_ID">{l s='Site ID' mod='payty'}</label>
        <div class="margin-form">
          <input type="text" id="PAYTY_SITE_ID" name="PAYTY_SITE_ID" value="{$PAYTY_SITE_ID|escape:'html':'UTF-8'}" autocomplete="off">
          <p>{l s='The identifier provided by your bank.' mod='payty'}</p>
        </div>

        {if !$payty_plugin_features['qualif']}
          <label for="PAYTY_KEY_TEST">{l s='Key in test mode' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_KEY_TEST" name="PAYTY_KEY_TEST" value="{$PAYTY_KEY_TEST|escape:'html':'UTF-8'}" autocomplete="off">
            <p>{l s='Key provided by your bank for test mode (available in your store Back Office).' mod='payty'}</p>
          </div>
        {/if}

        <label for="PAYTY_KEY_PROD">{l s='Key in production mode' mod='payty'}</label>
        <div class="margin-form">
          <input type="text" id="PAYTY_KEY_PROD" name="PAYTY_KEY_PROD" value="{$PAYTY_KEY_PROD|escape:'html':'UTF-8'}" autocomplete="off">
          <p>{l s='Key provided by your bank (available in your store Back Office after enabling production mode).' mod='payty'}</p>
        </div>

        <label for="PAYTY_MODE">{l s='Mode' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_MODE" name="PAYTY_MODE" {if $payty_plugin_features['qualif']} disabled="disabled"{/if}>
            {foreach from=$payty_mode_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_MODE === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='The context mode of this module.' mod='payty'}</p>
        </div>

        <label for="PAYTY_SIGN_ALGO">{l s='Signature algorithm' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_SIGN_ALGO" name="PAYTY_SIGN_ALGO">
            <option value="SHA-1"{if $PAYTY_SIGN_ALGO === 'SHA-1'} selected="selected"{/if}>SHA-1</option>
            <option value="SHA-256"{if $PAYTY_SIGN_ALGO === 'SHA-256'} selected="selected"{/if}>HMAC-SHA-256</option>
          </select>
          <p>
            {l s='Algorithm used to compute the payment form signature. Selected algorithm must be the same as one configured in your store Back Office.' mod='payty'}<br />
            {if !$payty_plugin_features['shatwo']}
              <b>{l s='The HMAC-SHA-256 algorithm should not be activated if it is not yet available in your store Back Office, the feature will be available soon.' mod='payty'}</b>
            {/if}
          </p>
        </div>

        <label>{l s='Instant Payment Notification URL' mod='payty'}</label>
        <div class="margin-form">
          <span style="font-weight: bold;">{$PAYTY_NOTIFY_URL|escape:'html':'UTF-8'}</span><br />
          <p>
            <img src="{$smarty.const._MODULE_DIR_|escape:'html':'UTF-8'}payty/views/img/warn.png">
            <span style="color: red; display: inline-block;">
              {l s='URL to copy into your bank Back Office > Settings > Notification rules.' mod='payty'}<br />
              {l s='In multistore mode, notification URL is the same for all the stores.' mod='payty'}
            </span>
          </p>
        </div>

        <label for="PAYTY_PLATFORM_URL">{l s='Payment page URL' mod='payty'}</label>
        <div class="margin-form">
          <input type="text" id="PAYTY_PLATFORM_URL" name="PAYTY_PLATFORM_URL" value="{$PAYTY_PLATFORM_URL|escape:'html':'UTF-8'}" style="width: 470px;">
          <p>{l s='Link to the payment page.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend onclick="javascript: paytyAdditionalOptionsToggle(this);" style="cursor: pointer;">
          <span class="ui-icon ui-icon-triangle-1-e" style="display: inline-block; vertical-align: middle;"></span>
          {l s='REST API KEYS' mod='payty'}
        </legend>

        <p style="font-size: .85em; color: #7F7F7F;">
         {l s='REST API keys are available in your store Back Office (menu: Settings > Shops > REST API keys).' mod='payty'}
        </p>

        <section style="display: none; padding-top: 15px;">
          <p style="font-size: .85em; color: #7F7F7F;">
           {l s='Configure this section if you are using order operations from Prestashop Back Office or if you are using embedded payment fields or popin modes.' mod='payty'}
          </p>
          <label for="PAYTY_PRIVKEY_TEST">{l s='Test password' mod='payty'}</label>
          <div class="margin-form">
            <input type="password" id="PAYTY_PRIVKEY_TEST" name="PAYTY_PRIVKEY_TEST" value="{$PAYTY_PRIVKEY_TEST|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off" />
          </div>
          <p></p>

          <label for="PAYTY_PRIVKEY_PROD">{l s='Production password' mod='payty'}</label>
          <div style="border-bottom: 5px;" class="margin-form">
            <input type="password" id="PAYTY_PRIVKEY_PROD" name="PAYTY_PRIVKEY_PROD" value="{$PAYTY_PRIVKEY_PROD|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off">
          </div>
          <p></p>

          <label for="PAYTY_REST_SERVER_URL">{l s='REST API server URL' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_REST_SERVER_URL" name="PAYTY_REST_SERVER_URL" value="{$PAYTY_REST_SERVER_URL|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off">
          </div>
          <p></p>

          <label for="PAYTY_ENABLE_WS">{l s='Enable web services' mod='payty'}</label>
          <div style="border-bottom: 5px;" class="margin-form">
            <input type="checkbox" id="PAYTY_ENABLE_WS" name="PAYTY_ENABLE_WS" value="enabled" {if ($PAYTY_ENABLE_WS === 'enabled')}checked{/if}>
             <p>
              {l s='Enable web services for order operations from PrestaShop Back Office.' mod='payty'}<br />
              {l s='If you keep the box unchecked, you will have to intervene directly on your store Back Office to manage these operations.' mod='payty'}
             </p>
          </div>

          <p style="font-size: .85em; color: #7F7F7F;">
           {l s='Configure this section only if you are using embedded payment fields or popin modes.' mod='payty'}
          </p>
          <p></p>

          <label for="PAYTY_PUBKEY_TEST">{l s='Public test key' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_PUBKEY_TEST" name="PAYTY_PUBKEY_TEST" value="{$PAYTY_PUBKEY_TEST|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off">
          </div>
          <p></p>

          <label for="PAYTY_PUBKEY_PROD">{l s='Public production key' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_PUBKEY_PROD" name="PAYTY_PUBKEY_PROD" value="{$PAYTY_PUBKEY_PROD|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off">
          </div>
          <p></p>

          <label for="PAYTY_RETKEY_TEST">{l s='HMAC-SHA-256 test key' mod='payty'}</label>
          <div class="margin-form">
            <input type="password" id="PAYTY_RETKEY_TEST" name="PAYTY_RETKEY_TEST" value="{$PAYTY_RETKEY_TEST|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off">
          </div>
          <p></p>

          <label for="PAYTY_RETKEY_PROD">{l s='HMAC-SHA-256 production key' mod='payty'}</label>
          <div class="margin-form">
            <input type="password" id="PAYTY_RETKEY_PROD" name="PAYTY_RETKEY_PROD" value="{$PAYTY_RETKEY_PROD|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off">
          </div>
          <p></p>

          <label>{l s='API REST Notification URL' mod='payty'}</label>
          <div class="margin-form">
            {$PAYTY_REST_NOTIFY_URL|escape:'html':'UTF-8'}<br />
            <p>
              <img src="{$smarty.const._MODULE_DIR_|escape:'html':'UTF-8'}payty/views/img/warn.png">
              <span style="color: red; display: inline-block;">
                {l s='URL to copy into your bank Back Office > Settings > Notification rules.' mod='payty'}<br />
                {l s='In multistore mode, notification URL is the same for all the stores.' mod='payty'}
              </span>
            </p>
          </div>

          <label for="PAYTY_REST_JS_CLIENT_URL">{l s='JavaScript client URL' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_REST_JS_CLIENT_URL" name="PAYTY_REST_JS_CLIENT_URL" value="{$PAYTY_REST_JS_CLIENT_URL|escape:'html':'UTF-8'}" style="width: 470px;" autocomplete="off">
          </div>
        </section>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

        <label for="PAYTY_DEFAULT_LANGUAGE">{l s='Default language' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_DEFAULT_LANGUAGE" name="PAYTY_DEFAULT_LANGUAGE">
            {foreach from=$payty_language_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_DEFAULT_LANGUAGE === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='Default language on the payment page.' mod='payty'}</p>
        </div>

        <label for="PAYTY_AVAILABLE_LANGUAGES">{l s='Available languages' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_AVAILABLE_LANGUAGES" name="PAYTY_AVAILABLE_LANGUAGES[]" multiple="multiple" size="8">
            {foreach from=$payty_language_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_AVAILABLE_LANGUAGES)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='Languages available on the payment page. If you do not select any, all the supported languages will be available.' mod='payty'}</p>
        </div>

        <label for="PAYTY_DELAY">{l s='Capture delay' mod='payty'}</label>
        <div class="margin-form">
          <input type="text" id="PAYTY_DELAY" name="PAYTY_DELAY" value="{$PAYTY_DELAY|escape:'html':'UTF-8'}">
          <p>{l s='The number of days before the bank capture (adjustable in your store Back Office).' mod='payty'}</p>
        </div>

        <label for="PAYTY_VALIDATION_MODE">{l s='Validation mode' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_VALIDATION_MODE" name="PAYTY_VALIDATION_MODE">
            {foreach from=$payty_validation_mode_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_VALIDATION_MODE === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='PAYMENT PAGE CUSTOMIZE' mod='payty'}</legend>

        <label>{l s='Theme configuration' mod='payty'}</label>
        <div class="margin-form">
          {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_THEME_CONFIG"
              input_value=$PAYTY_THEME_CONFIG
              style="width: 470px;"
           }
          <p>{l s='The theme configuration to customize the payment page.' mod='payty'}</p>
        </div>

        <label for="PAYTY_SHOP_NAME">{l s='Shop name' mod='payty'}</label>
        <div class="margin-form">
          <input type="text" id="PAYTY_SHOP_NAME" name="PAYTY_SHOP_NAME" value="{$PAYTY_SHOP_NAME|escape:'html':'UTF-8'}">
          <p>{l s='Shop name to display on the payment page. Leave blank to use gateway configuration.' mod='payty'}</p>
        </div>

        <label for="PAYTY_SHOP_URL">{l s='Shop URL' mod='payty'}</label>
        <div class="margin-form">
          <input type="text" id="PAYTY_SHOP_URL" name="PAYTY_SHOP_URL" value="{$PAYTY_SHOP_URL|escape:'html':'UTF-8'}" style="width: 470px;">
          <p>{l s='Shop URL to display on the payment page. Leave blank to use gateway configuration.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='CUSTOM 3DS' mod='payty'}</legend>

        <label for="PAYTY_3DS_MIN_AMOUNT">{l s='Manage 3DS by customer group' mod='payty'}</label>
        <div class="margin-form">
          {include file="./table_amount_group.tpl"
            groups=$prestashop_groups
            input_name="PAYTY_3DS_MIN_AMOUNT"
            input_value=$PAYTY_3DS_MIN_AMOUNT
            min_only=true
          }
          <p>{l s='Amount by customer group below which customer could be exempt from strong authentication. Needs subscription to « Selective 3DS1 » or « Frictionless 3DS2 » options. For more information, refer to the module documentation.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='RETURN TO SHOP' mod='payty'}</legend>

        <label for="PAYTY_REDIRECT_ENABLED">{l s='Automatic redirection' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_REDIRECT_ENABLED" name="PAYTY_REDIRECT_ENABLED" onchange="javascript: paytyRedirectChanged();">
            {foreach from=$payty_enable_disable_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_REDIRECT_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='If enabled, the buyer is automatically redirected to your site at the end of the payment.' mod='payty'}</p>
        </div>

        <section id="payty_redirect_settings">
          <label for="PAYTY_REDIRECT_SUCCESS_T">{l s='Redirection timeout on success' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_REDIRECT_SUCCESS_T" name="PAYTY_REDIRECT_SUCCESS_T" value="{$PAYTY_REDIRECT_SUCCESS_T|escape:'html':'UTF-8'}">
            <p>{l s='Time in seconds (0-300) before the buyer is automatically redirected to your website after a successful payment.' mod='payty'}</p>
          </div>

          <label>{l s='Redirection message on success' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_REDIRECT_SUCCESS_M"
              input_value=$PAYTY_REDIRECT_SUCCESS_M
              style="width: 470px;"
            }
            <p>{l s='Message displayed on the payment page prior to redirection after a successful payment.' mod='payty'}</p>
          </div>

          <label for="PAYTY_REDIRECT_ERROR_T">{l s='Redirection timeout on failure' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_REDIRECT_ERROR_T" name="PAYTY_REDIRECT_ERROR_T" value="{$PAYTY_REDIRECT_ERROR_T|escape:'html':'UTF-8'}">
            <p>{l s='Time in seconds (0-300) before the buyer is automatically redirected to your website after a declined payment.' mod='payty'}</p>
          </div>

          <label>{l s='Redirection message on failure' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_REDIRECT_ERROR_M"
              input_value=$PAYTY_REDIRECT_ERROR_M
              style="width: 470px;"
            }
            <p>{l s='Message displayed on the payment page prior to redirection after a declined payment.' mod='payty'}</p>
          </div>
        </section>

        <script type="text/javascript">
          paytyRedirectChanged();
        </script>

        <label for="PAYTY_RETURN_MODE">{l s='Return mode' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_RETURN_MODE" name="PAYTY_RETURN_MODE">
            <option value="GET"{if $PAYTY_RETURN_MODE === 'GET'} selected="selected"{/if}>GET</option>
            <option value="POST"{if $PAYTY_RETURN_MODE === 'POST'} selected="selected"{/if}>POST</option>
          </select>
          <p>{l s='Method that will be used for transmitting the payment result from the payment page to your shop.' mod='payty'}</p>
        </div>

        <label for="PAYTY_FAILURE_MANAGEMENT">{l s='Payment failed management' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_FAILURE_MANAGEMENT" name="PAYTY_FAILURE_MANAGEMENT">
            {foreach from=$payty_failure_management_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_FAILURE_MANAGEMENT === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='How to manage the buyer return to shop when the payment is failed.' mod='payty'}</p>
        </div>

        <label for="PAYTY_CART_MANAGEMENT">{l s='Cart management' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_CART_MANAGEMENT" name="PAYTY_CART_MANAGEMENT">
            {foreach from=$payty_cart_management_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_CART_MANAGEMENT === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='We recommend to choose the option « Empty cart » in order to avoid amount inconsistencies. In case of return back from the browser button the cart will be emptied. However in case of cancelled or refused payment, the cart will be recovered. If you do not want to have this behavior but the default PrestaShop one which is to keep the cart, choose the second option.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend onclick="javascript: paytyAdditionalOptionsToggle(this);" style="cursor: pointer;">
          <span class="ui-icon ui-icon-triangle-1-e" style="display: inline-block; vertical-align: middle;"></span>
          {l s='ADDITIONAL OPTIONS' mod='payty'}
        </legend>
        <p style="font-size: .85em; color: #7F7F7F;">{l s='Configure this section if you use advanced risk assessment module or if you have a FacilyPay Oney contract.' mod='payty'}</p>

        <section style="display: none; padding-top: 15px;">
          <label for="PAYTY_SEND_CART_DETAIL">{l s='Send shopping cart details' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_SEND_CART_DETAIL" name="PAYTY_SEND_CART_DETAIL">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SEND_CART_DETAIL === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='If you disable this option, the shopping cart details will not be sent to the gateway. Attention, in some cases, this option has to be enabled. For more information, refer to the module documentation.' mod='payty'}</p>
          </div>

          <label for="PAYTY_COMMON_CATEGORY">{l s='Category mapping' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_COMMON_CATEGORY" name="PAYTY_COMMON_CATEGORY" style="width: 220px;" onchange="javascript: paytyCategoryTableVisibility();">
              <option value="CUSTOM_MAPPING"{if $PAYTY_COMMON_CATEGORY === 'CUSTOM_MAPPING'} selected="selected"{/if}>{l s='(Use category mapping below)' mod='payty'}</option>
              {foreach from=$payty_category_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_COMMON_CATEGORY === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Use the same category for all products.' mod='payty'}</p>

            <table cellpadding="10" cellspacing="0" class="table payty_category_mapping" style="margin-top: 15px;{if $PAYTY_COMMON_CATEGORY != 'CUSTOM_MAPPING'} display: none;{/if}">
            <thead>
              <tr>
                <th>{l s='Product category' mod='payty'}</th>
                <th>{l s='Bank product category' mod='payty'}</th>
              </tr>
            </thead>
            <tbody>
              {foreach from=$prestashop_categories item="category"}
                {if $category.id_parent === 0}
                  {continue}
                {/if}

                {assign var="category_id" value=$category.id_category}

                {if isset($PAYTY_CATEGORY_MAPPING[$category_id])}
                  {assign var="exists" value=true}
                {else}
                  {assign var="exists" value=false}
                {/if}

                {if $exists}
                  {assign var="payty_category" value=$PAYTY_CATEGORY_MAPPING[$category_id]}
                {else}
                  {assign var="payty_category" value="FOOD_AND_GROCERY"}
                {/if}

                <tr id="payty_category_mapping_{$category_id|escape:'html':'UTF-8'}">
                  <td>{$category.name|escape:'html':'UTF-8'}{if $exists === false}<span style="color: red;">*</span>{/if}</td>
                  <td>
                    <select id="PAYTY_CATEGORY_MAPPING_{$category_id|escape:'html':'UTF-8'}" name="PAYTY_CATEGORY_MAPPING[{$category_id|escape:'html':'UTF-8'}]"
                        style="width: 220px;"{if $PAYTY_COMMON_CATEGORY != 'CUSTOM_MAPPING'} disabled="disabled"{/if}>
                      {foreach from=$payty_category_options key="key" item="option"}
                        <option value="{$key|escape:'html':'UTF-8'}"{if $payty_category === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                    </select>
                  </td>
                </tr>
              {/foreach}
            </tbody>
            </table>
            <p class="payty_category_mapping"{if $PAYTY_COMMON_CATEGORY != 'CUSTOM_MAPPING'} style="display: none;"{/if}>{l s='Match each product category with a bank product category.' mod='payty'} <b>{l s='Entries marked with * are newly added and must be configured.' mod='payty'}</b></p>
          </div>

          <label for="PAYTY_SEND_SHIP_DATA">{l s='Always send advanced shipping data' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_SEND_SHIP_DATA" name="PAYTY_SEND_SHIP_DATA">
              {foreach from=$payty_yes_no_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SEND_SHIP_DATA === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Select « Yes » to send advanced shipping data for all payments (carrier name, delivery type and delivery rapidity).' mod='payty'}</p>
          </div>

          <label>{l s='Shipping options' mod='payty'}</label>
          <div class="margin-form">
            <table class="table" cellpadding="10" cellspacing="0">
            <thead>
              <tr>
                <th>{l s='Method title' mod='payty'}</th>
                <th>{l s='Name' mod='payty'}</th>
                <th>{l s='Type' mod='payty'}</th>
                <th>{l s='Rapidity' mod='payty'}</th>
                <th>{l s='Delay' mod='payty'}</th>
                <th style="width: 270px;" colspan="3">{l s='Address' mod='payty'}</th>
              </tr>
            </thead>
            <tbody>
              {foreach from=$prestashop_carriers item="carrier"}
                {assign var="carrier_id" value=$carrier.id_carrier}

                {if isset($PAYTY_ONEY_SHIP_OPTIONS[$carrier_id])}
                  {assign var="exists" value=true}
                {else}
                  {assign var="exists" value=false}
                {/if}

                {if $exists}
                  {assign var="ship_option" value=$PAYTY_ONEY_SHIP_OPTIONS[$carrier_id]}
                {/if}

                <tr>
                  <td>{$carrier.name|escape:'html':'UTF-8'}{if $exists === false}<span style="color: red;">*</span>{/if}</td>
                  <td>
                    <input id="PAYTY_ONEY_SHIP_OPTIONS_{$carrier_id|escape:'html':'UTF-8'}_label"
                        name="PAYTY_ONEY_SHIP_OPTIONS[{$carrier_id|escape:'html':'UTF-8'}][label]"
                        value="{if isset($ship_option)}{$ship_option.label|escape:'html':'UTF-8'}{else}{$carrier.name|regex_replace:"#[^A-Z0-9ÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ /'-]#ui":" "|substr:0:55}{/if}"
                        type="text">
                  </td>
                  <td>
                    <select id="PAYTY_ONEY_SHIP_OPTIONS_{$carrier_id|escape:'html':'UTF-8'}_type" name="PAYTY_ONEY_SHIP_OPTIONS[{$carrier_id|escape:'html':'UTF-8'}][type]" onchange="javascript: paytyDeliveryTypeChanged({$carrier_id|escape:'html':'UTF-8'});" style="width: 150px;">
                      {foreach from=$payty_delivery_type_options key="key" item="option"}
                        <option value="{$key|escape:'html':'UTF-8'}"{if (isset($ship_option) && $ship_option.type === $key) || ('PACKAGE_DELIVERY_COMPANY' === $key)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                    </select>
                  </td>
                  <td>
                    <select id="PAYTY_ONEY_SHIP_OPTIONS_{$carrier_id|escape:'html':'UTF-8'}_speed" name="PAYTY_ONEY_SHIP_OPTIONS[{$carrier_id|escape:'html':'UTF-8'}][speed]" onchange="javascript: paytyDeliverySpeedChanged({$carrier_id|escape:'html':'UTF-8'});">
                      {foreach from=$payty_delivery_speed_options key="key" item="option"}
                        <option value="{$key|escape:'html':'UTF-8'}"{if (isset($ship_option) && $ship_option.speed === $key) || ('STANDARD' === $key)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                    </select>
                  </td>
                  <td>
                    <select
                        id="PAYTY_ONEY_SHIP_OPTIONS_{$carrier_id|escape:'html':'UTF-8'}_delay"
                        name="PAYTY_ONEY_SHIP_OPTIONS[{$carrier_id|escape:'html':'UTF-8'}][delay]"
                        style="{if !isset($ship_option) || ($ship_option.type != 'RECLAIM_IN_SHOP') || ($ship_option.speed != 'PRIORITY')} display: none;{/if}">
                      {foreach from=$payty_delivery_delay_options key="key" item="option"}
                        <option value="{$key|escape:'html':'UTF-8'}"{if (isset($ship_option) && isset($ship_option.delay) && ($ship_option.delay === $key)) || 'INFERIOR_EQUALS' === $key} selected="selected"{/if}>{$option|escape:'quotes':'UTF-8'}</option>
                      {/foreach}
                    </select>
                  </td>
                  <td>
                    <input
                        id="PAYTY_ONEY_SHIP_OPTIONS_{$carrier_id|escape:'html':'UTF-8'}_address"
                        name="PAYTY_ONEY_SHIP_OPTIONS[{$carrier_id|escape:'html':'UTF-8'}][address]"
                        placeholder="{l s='Address' mod='payty'}"
                        value="{if isset($ship_option)}{$ship_option.address|escape:'html':'UTF-8'}{/if}"
                        style="width: 160px;{if !isset($ship_option) || $ship_option.type != 'RECLAIM_IN_SHOP'} display: none;{/if}"
                        type="text">
                  </td>
                  <td>
                    <input
                        id="PAYTY_ONEY_SHIP_OPTIONS_{$carrier_id|escape:'html':'UTF-8'}_zip"
                        name="PAYTY_ONEY_SHIP_OPTIONS[{$carrier_id|escape:'html':'UTF-8'}][zip]"
                        placeholder="{l s='Zip code' mod='payty'}"
                        value="{if isset($ship_option)}{$ship_option.zip|escape:'html':'UTF-8'}{/if}"
                        style="width: 50px;{if !isset($ship_option) || $ship_option.type != 'RECLAIM_IN_SHOP'} display: none;{/if}"
                        type="text">
                  </td>
                  <td>
                    <input
                        id="PAYTY_ONEY_SHIP_OPTIONS_{$carrier_id|escape:'html':'UTF-8'}_city"
                        name="PAYTY_ONEY_SHIP_OPTIONS[{$carrier_id|escape:'html':'UTF-8'}][city]"
                        placeholder="{l s='City' mod='payty'}"
                        value="{if isset($ship_option)}{$ship_option.city|escape:'html':'UTF-8'}{/if}"
                        style="width: 160px;{if !isset($ship_option) || $ship_option.type != 'RECLAIM_IN_SHOP'} display: none;{/if}"
                        type="text">
                  </td>
                </tr>
              {/foreach}
            </tbody>
            </table>
            <p>
              {l s='Define the information about all shipping methods.' mod='payty'}<br />
              <b>{l s='Name' mod='payty'} : </b>{l s='The label of the shipping method (use 55 alphanumeric characters, accentuated characters and these special characters: space, slash, hyphen, apostrophe).' mod='payty'}<br />
              <b>{l s='Type' mod='payty'} : </b>{l s='The delivery type of shipping method.' mod='payty'}<br />
              <b>{l s='Rapidity' mod='payty'} : </b>{l s='Select the delivery rapidity.' mod='payty'}<br />
              <b>{l s='Delay' mod='payty'} : </b>{l s='Select the delivery delay if speed is « Priority ».' mod='payty'}<br />
              <b>{l s='Address' mod='payty'} : </b>{l s='Enter address if it is a reclaim in shop.' mod='payty'}<br />
              <b>{l s='Entries marked with * are newly added and must be configured.' mod='payty'}</b>
            </p>
          </div>
        </section>
      </fieldset>
      <div class="clear">&nbsp;</div>
    </div>

    <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
      <a href="#">{l s='STANDARD PAYMENT' mod='payty'}</a>
    </h4>
    <div>
      <fieldset>
        <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

       <label for="PAYTY_STD_ENABLED">{l s='Activation' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_STD_ENABLED" name="PAYTY_STD_ENABLED">
            {foreach from=$payty_enable_disable_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
        </div>

        <label>{l s='Payment method title' mod='payty'}</label>
        <div class="margin-form">
          {include file="./input_text_lang.tpl"
            languages=$prestashop_languages
            current_lang=$prestashop_lang
            input_name="PAYTY_STD_TITLE"
            input_value=$PAYTY_STD_TITLE
            style="width: 330px;"
          }
          <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

        <label for="PAYTY_STD_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_STD_COUNTRY" name="PAYTY_STD_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_STD_COUNTRY')">
            {foreach from=$payty_countries_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
        </div>

        <div id="PAYTY_STD_COUNTRY_MENU" {if $PAYTY_STD_COUNTRY === '1'} style="display: none;"{/if}>
          <label for="PAYTY_STD_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_STD_COUNTRY_LST" name="PAYTY_STD_COUNTRY_LST[]" multiple="multiple" size="7">
              {foreach from=$payty_countries_list['ps_countries'] key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_STD_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
          </div>
        </div>

        <label>{l s='Customer group amount restriction' mod='payty'}</label>
        <div class="margin-form">
          {include file="./table_amount_group.tpl"
            groups=$prestashop_groups
            input_name="PAYTY_STD_AMOUNTS"
            input_value=$PAYTY_STD_AMOUNTS
          }
          <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

        <label for="PAYTY_STD_DELAY">{l s='Capture delay' mod='payty'}</label>
        <div class="margin-form">
          <input id="PAYTY_STD_DELAY" name="PAYTY_STD_DELAY" value="{$PAYTY_STD_DELAY|escape:'html':'UTF-8'}" type="text">
          <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
        </div>

        <label for="PAYTY_STD_VALIDATION">{l s='Validation mode' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_STD_VALIDATION" name="PAYTY_STD_VALIDATION">
            <option value="-1"{if $PAYTY_STD_VALIDATION === '-1'} selected="selected"{/if}>{l s='Base settings configuration' mod='payty'}</option>
            {foreach from=$payty_validation_mode_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_VALIDATION === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
        </div>

        <label for="PAYTY_STD_PAYMENT_CARDS">{l s='Card Types' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_STD_PAYMENT_CARDS" name="PAYTY_STD_PAYMENT_CARDS[]" multiple="multiple" size="7">
            {foreach from=$payty_payment_cards_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_STD_PAYMENT_CARDS)} selected="selected"{/if}>{if $key !== ""} {$key|escape:'html':'UTF-8'} - {/if}{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='The card type(s) that can be used for the payment. Select none to use gateway configuration.' mod='payty'}</p>
        </div>

        {if $payty_plugin_features['oney']}
          <label for="PAYTY_STD_PROPOSE_ONEY">{l s='Propose FacilyPay Oney' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_STD_PROPOSE_ONEY" name="PAYTY_STD_PROPOSE_ONEY">
              {foreach from=$payty_yes_no_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_PROPOSE_ONEY === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Select « Yes » if you want to propose FacilyPay Oney in standard payment. Attention, you must ensure that you have a FacilyPay Oney contract.' mod='payty'}</p>
          </div>
        {/if}
        </fieldset>
        <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='ADVANCED OPTIONS' mod='payty'}</legend>

        <label for="PAYTY_STD_CARD_DATA_MODE">{l s='Card data entry mode' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_STD_CARD_DATA_MODE" name="PAYTY_STD_CARD_DATA_MODE" onchange="javascript: paytyCardEntryChanged();">
            {foreach from=$payty_card_data_mode_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_CARD_DATA_MODE === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <input type="hidden" id="PAYTY_STD_CARD_DATA_MODE_OLD" name="PAYTY_STD_CARD_DATA_MODE_OLD" value="{$PAYTY_STD_CARD_DATA_MODE|escape:'html':'UTF-8'}"/>
          <p>{l s='Select how the card data will be entered. Attention, to use bank data acquisition on the merchant site, you must ensure that you have subscribed to this option with your bank.' mod='payty'}</p>
        </div>

        <div id="PAYTY_STD_CANCEL_IFRAME_MENU" {if $PAYTY_STD_CARD_DATA_MODE !== '4'} style="display: none;"{/if}>
          <label for="PAYTY_STD_CANCEL_IFRAME">{l s='Cancel payment in iframe mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_STD_CANCEL_IFRAME" name="PAYTY_STD_CANCEL_IFRAME">
              {foreach from=$payty_yes_no_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_CANCEL_IFRAME === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Select « Yes » if you want to propose payment cancellation in iframe mode.' sprintf='Payty' mod='payty'}</p>
          </div>
        </div>

        <div id="PAYTY_REST_SETTINGS" {if $PAYTY_STD_CARD_DATA_MODE != '5' && $PAYTY_STD_CARD_DATA_MODE != '6'} style="display: none;"{/if}>
          <label for="PAYTY_STD_REST_THEME">{l s='Theme' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_STD_REST_THEME" name="PAYTY_STD_REST_THEME">
              {foreach from=$payty_std_rest_theme_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_REST_THEME === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Select a theme to use to display embedded payment fields. For more customization, you can edit module template manually.' mod='payty'}</p>
          </div>
          <p></p>

          <label for="PAYTY_STD_REST_PLACEHLDR">{l s='Custom fields placeholders' mod='payty'}</label>
          <div class="margin-form">
            <table class="table" cellspacing="0" cellpadding="10">
              <tbody>
                <tr>
                  <td>{l s='Card number' mod='payty'}</td>
                  <td>
                    {include file="./input_text_lang.tpl"
                      languages=$prestashop_languages
                      current_lang=$prestashop_lang
                      input_name="PAYTY_STD_REST_PLACEHLDR[pan]"
                      field_id="PAYTY_STD_REST_PLACEHLDR_pan"
                      input_value=$PAYTY_STD_REST_PLACEHLDR.pan
                      style="width: 150px;"
                    }
                  </td>
                </tr>

                <tr>
                  <td>{l s='Expiry date' mod='payty'}</td>
                  <td>
                    {include file="./input_text_lang.tpl"
                      languages=$prestashop_languages
                      current_lang=$prestashop_lang
                      input_name="PAYTY_STD_REST_PLACEHLDR[expiry]"
                      field_id="PAYTY_STD_REST_PLACEHLDR_expiry"
                      input_value=$PAYTY_STD_REST_PLACEHLDR.expiry
                      style="width: 150px;"
                    }
                  </td>
                </tr>

                <tr>
                  <td>{l s='CVV' mod='payty'}</td>
                  <td>
                    {include file="./input_text_lang.tpl"
                      languages=$prestashop_languages
                      current_lang=$prestashop_lang
                      input_name="PAYTY_STD_REST_PLACEHLDR[cvv]"
                      field_id="PAYTY_STD_REST_PLACEHLDR_cvv"
                      input_value=$PAYTY_STD_REST_PLACEHLDR.cvv
                      style="width: 150px;"
                    }
                  </td>
                </tr>

              </tbody>
            </table>
            <p>{l s='Texts to use as placeholders for embedded payment fields.' mod='payty'}</p>
          </div>
          <p></p>

          <label>{l s='Register card label' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_STD_REST_LBL_REGIST"
              input_value=$PAYTY_STD_REST_LBL_REGIST
              style="width: 330px;"
            }
            <p>{l s='Label displayed to invite buyers to register their card data.' mod='payty'}</p>
          </div>
          <p></p>

          <label for="PAYTY_STD_REST_ATTEMPTS">{l s='Payment attempts number' mod='payty'}</label>
          <div class="margin-form">
            <input type="text" id="PAYTY_STD_REST_ATTEMPTS" name="PAYTY_STD_REST_ATTEMPTS" value="{$PAYTY_STD_REST_ATTEMPTS|escape:'html':'UTF-8'}" style="width: 150px;" />
            <p>{l s='Maximum number of payment retries after a failed payment (between 0 and 9). If blank, the gateway default value is 3.' mod='payty'}</p>
          </div>
          <p></p>

        </div>

        <div id="PAYTY_STD_1_CLICK_PAYMENT_MENU">
          <label for="PAYTY_STD_1_CLICK_PAYMENT">{l s='Payment by token' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_STD_1_CLICK_PAYMENT" name="PAYTY_STD_1_CLICK_PAYMENT">
              {foreach from=$payty_yes_no_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_STD_1_CLICK_PAYMENT === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='The payment by token allows to pay orders without re-entering bank data at each payment. The "payment by token" option should be enabled on your %s store to use this feature.' sprintf='Payty' mod='payty'}</p>
          </div>

        </div>

      </fieldset>
      <div class="clear">&nbsp;</div>
    </div>

    {if $payty_plugin_features['multi']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='PAYMENT IN INSTALLMENTS' mod='payty'}</a>
      </h4>
      <div>
        {if $payty_plugin_features['restrictmulti']}
          <p style="background: none repeat scroll 0 0 #FFFFE0; border: 1px solid #E6DB55; font-size: 13px; margin: 0 0 20px; padding: 10px;">
            {l s='ATTENTION: The payment in installments feature activation is subject to the prior agreement of Société Générale.' mod='payty'}<br />
            {l s='If you enable this feature while you have not the associated option, an error 10000 – INSTALLMENTS_NOT_ALLOWED or 07 - PAYMENT_CONFIG will occur and the buyer will not be able to pay.' mod='payty'}
          </p>
        {/if}

        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_MULTI_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_MULTI_ENABLED" name="PAYTY_MULTI_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_MULTI_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_MULTI_TITLE"
              input_value=$PAYTY_MULTI_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          <label for="PAYTY_MULTI_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_MULTI_COUNTRY" name="PAYTY_MULTI_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_MULTI_COUNTRY')">
              {foreach from=$payty_countries_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_MULTI_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
          </div>

          <div id="PAYTY_MULTI_COUNTRY_MENU" {if $PAYTY_MULTI_COUNTRY === '1'} style="display: none;"{/if}>
            <label for="PAYTY_MULTI_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_MULTI_COUNTRY_LST" name="PAYTY_MULTI_COUNTRY_LST[]" multiple="multiple" size="7">
                {foreach from=$payty_countries_list['ps_countries'] key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_MULTI_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
            </div>
          </div>

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_MULTI_AMOUNTS"
              input_value=$PAYTY_MULTI_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

          <label for="PAYTY_MULTI_DELAY">{l s='Capture delay' mod='payty'}</label>
          <div class="margin-form">
            <input id="PAYTY_MULTI_DELAY" name="PAYTY_MULTI_DELAY" value="{$PAYTY_MULTI_DELAY|escape:'html':'UTF-8'}" type="text">
            <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
          </div>

          <label for="PAYTY_MULTI_VALIDATION">{l s='Validation mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_MULTI_VALIDATION" name="PAYTY_MULTI_VALIDATION">
              <option value="-1"{if $PAYTY_MULTI_VALIDATION === '-1'} selected="selected"{/if}>{l s='Base settings configuration' mod='payty'}</option>
              {foreach from=$payty_validation_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_MULTI_VALIDATION === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
          </div>

          <label for="PAYTY_MULTI_PAYMENT_CARDS">{l s='Card Types' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_MULTI_PAYMENT_CARDS" name="PAYTY_MULTI_PAYMENT_CARDS[]" multiple="multiple" size="7">
              {foreach from=$payty_multi_payment_cards_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_MULTI_PAYMENT_CARDS)} selected="selected"{/if}>{if $key !== ""} {$key|escape:'html':'UTF-8'} - {/if}{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='The card type(s) that can be used for the payment. Select none to use gateway configuration.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='ADVANCED OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_MULTI_CARD_MODE">{l s='Card type selection' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_MULTI_CARD_MODE" name="PAYTY_MULTI_CARD_MODE">
              {foreach from=$payty_card_selection_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_MULTI_CARD_MODE === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Select where the card type will be selected by the buyer.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

          <label>{l s='Payment options' mod='payty'}</label>
          <div class="margin-form">
            <script type="text/html" id="payty_multi_row_option">
              {include file="./row_multi_option.tpl"
                languages=$prestashop_languages
                current_lang=$prestashop_lang
                key="PAYTY_MULTI_KEY"
                option=$payty_default_multi_option
              }
            </script>

            <button type="button" id="payty_multi_options_btn"{if !empty($PAYTY_MULTI_OPTIONS)} style="display: none;"{/if} onclick="javascript: paytyAddMultiOption(true, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>

            <table id="payty_multi_options_table"{if empty($PAYTY_MULTI_OPTIONS)} style="display: none;"{/if} class="table" cellpadding="10" cellspacing="0">
              <thead>
                <tr>
                  <th style="font-size: 10px;">{l s='Label' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Min amount' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Max amount' mod='payty'}</th>
                  {if in_array('CB', $payty_multi_payment_cards_options)}
                    <th style="font-size: 10px;">{l s='Contract' mod='payty'}</th>
                  {/if}
                  <th style="font-size: 10px;">{l s='Count' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Period' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='1st payment' mod='payty'}</th>
                  <th style="font-size: 10px;"></th>
                </tr>
              </thead>

              <tbody>
                {foreach from=$PAYTY_MULTI_OPTIONS key="key" item="option"}
                  {include file="./row_multi_option.tpl"
                    languages=$prestashop_languages
                    current_lang=$prestashop_lang
                    key=$key
                    option=$option
                  }
                {/foreach}

                <tr id="payty_multi_option_add">
                  <td colspan="{if in_array('CB', $payty_multi_payment_cards_options)}7{else}6{/if}"></td>
                  <td>
                    <button type="button" onclick="javascript: paytyAddMultiOption(false, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>
                  </td>
                </tr>
              </tbody>
            </table>
            <p>
              {l s='Click on « Add » button to configure one or more payment options.' mod='payty'}<br />
              <b>{l s='Label' mod='payty'} : </b>{l s='The option label to display on the frontend.' mod='payty'}<br />
              <b>{l s='Min amount' mod='payty'} : </b>{l s='Minimum amount to enable the payment option.' mod='payty'}<br />
              <b>{l s='Max amount' mod='payty'} : </b>{l s='Maximum amount to enable the payment option.' mod='payty'}<br />
              {if in_array('CB', $payty_multi_payment_cards_options)}
                <b>{l s='Contract' mod='payty'} : </b>{l s='ID of the contract to use with the option (Leave blank preferably).' mod='payty'}<br />
              {/if}
              <b>{l s='Count' mod='payty'} : </b>{l s='Total number of payments.' mod='payty'}<br />
              <b>{l s='Period' mod='payty'} : </b>{l s='Delay (in days) between payments.' mod='payty'}<br />
              <b>{l s='1st payment' mod='payty'} : </b>{l s='Amount of first payment, in percentage of total amount. If empty, all payments will have the same amount.' mod='payty'}<br />
              <b>{l s='Do not forget to click on « Save » button to save your modifications.' mod='payty'}</b>
            </p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['choozeo']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='CHOOZEO PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_CHOOZEO_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_CHOOZEO_ENABLED" name="PAYTY_CHOOZEO_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_CHOOZEO_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_CHOOZEO_TITLE"
              input_value=$PAYTY_CHOOZEO_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          {if isset ($payty_countries_list['CHOOZEO'])}
            <label for="PAYTY_CHOOZEO_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_CHOOZEO_COUNTRY" name="PAYTY_CHOOZEO_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_CHOOZEO_COUNTRY')">
                {foreach from=$payty_countries_options key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_CHOOZEO_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
              <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
            </div>

            <div id="PAYTY_CHOOZEO_COUNTRY_MENU" {if $PAYTY_CHOOZEO_COUNTRY === '1'} style="display: none;"{/if}>
              <label for="PAYTY_CHOOZEO_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
              <div class="margin-form">
                <select id="PAYTY_CHOOZEO_COUNTRY_LST" name="PAYTY_CHOOZEO_COUNTRY_LST[]" multiple="multiple" size="7">
                  {if isset ($payty_countries_list['CHOOZEO'])}
                      {foreach from=$payty_countries_list['CHOOZEO'] key="key" item="option"}
                          <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_CHOOZEO_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                  {/if}
                </select>
              </div>
            </div>
          {else}
            <input type="hidden" name ="PAYTY_CHOOZEO_COUNTRY" value="1">
            <input type="hidden" name ="PAYTY_CHOOZEO_COUNTRY_LST[]" value ="">
            <p style="background: none repeat scroll 0 0 #FFFFE0; border: 1px solid #E6DB55; font-size: 13px; margin: 0 0 20px; padding: 10px;">
                {l s='Payment method unavailable for the list of countries defined on your PrestaShop store.' mod='payty'}
            </p>
          {/if}

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_CHOOZEO_AMOUNTS"
              input_value=$PAYTY_CHOOZEO_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

          <label for="PAYTY_CHOOZEO_DELAY">{l s='Capture delay' mod='payty'}</label>
          <div class="margin-form">
            <input id="PAYTY_CHOOZEO_DELAY" name="PAYTY_CHOOZEO_DELAY" value="{$PAYTY_CHOOZEO_DELAY|escape:'html':'UTF-8'}" type="text">
            <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

          <label>{l s='Payment options' mod='payty'}</label>
          <div class="margin-form">
            <table class="table" cellpadding="10" cellspacing="0">
              <thead>
                <tr>
                  <th>{l s='Activation' mod='payty'}</th>
                  <th>{l s='Label' mod='payty'}</th>
                  <th>{l s='Min amount' mod='payty'}</th>
                  <th>{l s='Max amount' mod='payty'}</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>
                    <input name="PAYTY_CHOOZEO_OPTIONS[EPNF_3X][enabled]"
                      style="width: 100%;"
                      type="checkbox"
                      value="True"
                      {if !isset($PAYTY_CHOOZEO_OPTIONS.EPNF_3X.enabled) || ($PAYTY_CHOOZEO_OPTIONS.EPNF_3X.enabled ==='True')}checked{/if}>
                  </td>
                  <td>Choozeo 3X CB</td>
                  <td>
                    <input name="PAYTY_CHOOZEO_OPTIONS[EPNF_3X][min_amount]"
                      value="{if isset($PAYTY_CHOOZEO_OPTIONS['EPNF_3X'])}{$PAYTY_CHOOZEO_OPTIONS['EPNF_3X']['min_amount']|escape:'html':'UTF-8'}{/if}"
                      style="width: 200px;"
                      type="text">
                  </td>
                  <td>
                    <input name="PAYTY_CHOOZEO_OPTIONS[EPNF_3X][max_amount]"
                      value="{if isset($PAYTY_CHOOZEO_OPTIONS['EPNF_3X'])}{$PAYTY_CHOOZEO_OPTIONS['EPNF_3X']['max_amount']|escape:'html':'UTF-8'}{/if}"
                      style="width: 200px;"
                      type="text">
                  </td>
                </tr>

                <tr>
                  <td>
                    <input name="PAYTY_CHOOZEO_OPTIONS[EPNF_4X][enabled]"
                      style="width: 100%;"
                      type="checkbox"
                      value="True"
                      {if !isset($PAYTY_CHOOZEO_OPTIONS.EPNF_4X.enabled) || ($PAYTY_CHOOZEO_OPTIONS.EPNF_4X.enabled ==='True')}checked{/if}>
                  </td>
                  <td>Choozeo 4X CB</td>
                  <td>
                    <input name="PAYTY_CHOOZEO_OPTIONS[EPNF_4X][min_amount]"
                      value="{if isset($PAYTY_CHOOZEO_OPTIONS['EPNF_4X'])}{$PAYTY_CHOOZEO_OPTIONS['EPNF_4X']['min_amount']|escape:'html':'UTF-8'}{/if}"
                      style="width: 200px;"
                      type="text">
                  </td>
                  <td>
                    <input name="PAYTY_CHOOZEO_OPTIONS[EPNF_4X][max_amount]"
                      value="{if isset($PAYTY_CHOOZEO_OPTIONS['EPNF_4X'])}{$PAYTY_CHOOZEO_OPTIONS['EPNF_4X']['max_amount']|escape:'html':'UTF-8'}{/if}"
                      style="width: 200px;"
                      type="text">
                  </td>
                </tr>
              </tbody>
            </table>
            <p>{l s='Define amount restriction for each card.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['oney']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='PAYMENT IN 3 OR 4 TIMES ONEY' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_ONEY34_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ONEY34_ENABLED" name="PAYTY_ONEY34_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ONEY34_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_ONEY34_TITLE"
              input_value=$PAYTY_ONEY34_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          {if isset ($payty_countries_list['ONEY'])}
            <label for="PAYTY_ONEY34_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_ONEY34_COUNTRY" name="PAYTY_ONEY34_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_ONEY34_COUNTRY')">
                {foreach from=$payty_countries_options key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ONEY34_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
              <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
            </div>

            <div id="PAYTY_ONEY34_COUNTRY_MENU" {if $PAYTY_ONEY34_COUNTRY === '1'} style="display: none;"{/if}>
              <label for="PAYTY_ONEY34_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
              <div class="margin-form">
                <select id="PAYTY_ONEY34_COUNTRY_LST" name="PAYTY_ONEY34_COUNTRY_LST[]" multiple="multiple" size="7">
                  {if isset ($payty_countries_list['ONEY'])}
                      {foreach from=$payty_countries_list['ONEY'] key="key" item="option"}
                          <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_ONEY34_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                  {/if}
                </select>
              </div>
            </div>
          {else}
            <input type="hidden" name ="PAYTY_ONEY34_COUNTRY" value="1">
            <input type="hidden" name ="PAYTY_ONEY34_COUNTRY_LST[]" value ="">
            <p style="background: none repeat scroll 0 0 #FFFFE0; border: 1px solid #E6DB55; font-size: 13px; margin: 0 0 20px; padding: 10px;">
                {l s='Payment method unavailable for the list of countries defined on your PrestaShop store.' mod='payty'}
            </p>
          {/if}

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_ONEY34_AMOUNTS"
              input_value=$PAYTY_ONEY34_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

          <label for="PAYTY_ONEY34_DELAY">{l s='Capture delay' mod='payty'}</label>
          <div class="margin-form">
            <input id="PAYTY_ONEY34_DELAY" name="PAYTY_ONEY34_DELAY" value="{$PAYTY_ONEY34_DELAY|escape:'html':'UTF-8'}" type="text">
            <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
          </div>

          <label for="PAYTY_ONEY34_VALIDATION">{l s='Validation mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ONEY34_VALIDATION" name="PAYTY_ONEY34_VALIDATION">
              <option value="-1"{if $PAYTY_ONEY34_VALIDATION === '-1'} selected="selected"{/if}>{l s='Base settings configuration' mod='payty'}</option>
              {foreach from=$payty_validation_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ONEY34_VALIDATION === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

          <label>{l s='Payment options' mod='payty'}</label>
          <div class="margin-form">
            <script type="text/html" id="payty_oney34_row_option">
              {include file="./row_oney_option.tpl"
                languages=$prestashop_languages
                current_lang=$prestashop_lang
                key="PAYTY_ONEY34_KEY"
                option=$payty_default_oney_option
                suffix='34'
              }
            </script>

            <button type="button" id="payty_oney34_options_btn"{if !empty($PAYTY_ONEY34_OPTIONS)} style="display: none;"{/if} onclick="javascript: paytyAddOneyOption(true, '34');">{l s='Add' mod='payty'}</button>

            <table id="payty_oney34_options_table"{if empty($PAYTY_ONEY34_OPTIONS)} style="display: none;"{/if} class="table" cellpadding="10" cellspacing="0">
              <thead>
                <tr>
                  <th style="font-size: 10px;">{l s='Label' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Code' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Min amount' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Max amount' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Count' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Rate' mod='payty'}</th>
                  <th style="font-size: 10px;"></th>
                </tr>
              </thead>

              <tbody>
                {foreach from=$PAYTY_ONEY34_OPTIONS key="key" item="option"}
                  {include file="./row_oney_option.tpl"
                    languages=$prestashop_languages
                    current_lang=$prestashop_lang
                    key=$key
                    option=$option
                    suffix='34'
                  }
                {/foreach}

                <tr id="payty_oney34_option_add">
                  <td colspan="6"></td>
                  <td>
                    <button type="button" onclick="javascript: paytyAddOneyOption(false, '34');">{l s='Add' mod='payty'}</button>
                  </td>
                </tr>
              </tbody>
            </table>
            <p>
              {l s='Click on « Add » button to configure one or more payment options.' mod='payty'}<br />
              <b>{l s='Label' mod='payty'} : </b>{l s='The option label to display on the frontend (the %c and %r patterns will be respectively replaced by payments count and option rate).' mod='payty'}<br />
              <b>{l s='Code' mod='payty'} : </b>{l s='The option code as defined in your Oney contract.' mod='payty'}<br />
              <b>{l s='Min amount' mod='payty'} : </b>{l s='Minimum amount to enable the payment option.' mod='payty'}<br />
              <b>{l s='Max amount' mod='payty'} : </b>{l s='Maximum amount to enable the payment option.' mod='payty'}<br />
              <b>{l s='Count' mod='payty'} : </b>{l s='Total number of payments.' mod='payty'}<br />
              <b>{l s='Rate' mod='payty'} : </b>{l s='The interest rate in percentage.' mod='payty'}<br />
              <b>{l s='Do not forget to click on « Save » button to save your modifications.' mod='payty'}</b>
            </p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['oney']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='FACILYPAY ONEY PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_ONEY_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ONEY_ENABLED" name="PAYTY_ONEY_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ONEY_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_ONEY_TITLE"
              input_value=$PAYTY_ONEY_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          {if isset ($payty_countries_list['ONEY'])}
            <label for="PAYTY_ONEY_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_ONEY_COUNTRY" name="PAYTY_ONEY_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_ONEY_COUNTRY')">
                {foreach from=$payty_countries_options key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ONEY_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
              <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
            </div>

            <div id="PAYTY_ONEY_COUNTRY_MENU" {if $PAYTY_ONEY_COUNTRY === '1'} style="display: none;"{/if}>
              <label for="PAYTY_ONEY_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
              <div class="margin-form">
                <select id="PAYTY_ONEY_COUNTRY_LST" name="PAYTY_ONEY_COUNTRY_LST[]" multiple="multiple" size="7">
                  {if isset ($payty_countries_list['ONEY'])}
                      {foreach from=$payty_countries_list['ONEY'] key="key" item="option"}
                          <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_ONEY_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                  {/if}
                </select>
              </div>
            </div>
          {else}
            <input type="hidden" name ="PAYTY_ONEY_COUNTRY" value="1">
            <input type="hidden" name ="PAYTY_ONEY_COUNTRY_LST[]" value ="">
            <p style="background: none repeat scroll 0 0 #FFFFE0; border: 1px solid #E6DB55; font-size: 13px; margin: 0 0 20px; padding: 10px;">
                {l s='Payment method unavailable for the list of countries defined on your PrestaShop store.' mod='payty'}
            </p>
          {/if}

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_ONEY_AMOUNTS"
              input_value=$PAYTY_ONEY_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

          <label for="PAYTY_ONEY_DELAY">{l s='Capture delay' mod='payty'}</label>
          <div class="margin-form">
            <input id="PAYTY_ONEY_DELAY" name="PAYTY_ONEY_DELAY" value="{$PAYTY_ONEY_DELAY|escape:'html':'UTF-8'}" type="text">
            <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
          </div>

          <label for="PAYTY_ONEY_VALIDATION">{l s='Validation mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ONEY_VALIDATION" name="PAYTY_ONEY_VALIDATION">
              <option value="-1"{if $PAYTY_ONEY_VALIDATION === '-1'} selected="selected"{/if}>{l s='Base settings configuration' mod='payty'}</option>
              {foreach from=$payty_validation_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ONEY_VALIDATION === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_ONEY_ENABLE_OPTIONS">{l s='Enable options selection' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ONEY_ENABLE_OPTIONS" name="PAYTY_ONEY_ENABLE_OPTIONS" onchange="javascript: paytyOneyEnableOptionsChanged();">
              {foreach from=$payty_yes_no_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ONEY_ENABLE_OPTIONS === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enable payment options selection on merchant site.' mod='payty'}</p>
          </div>

          <section id="payty_oney_options_settings">
            <label>{l s='Payment options' mod='payty'}</label>
            <div class="margin-form">
              <script type="text/html" id="payty_oney_row_option">
                {include file="./row_oney_option.tpl"
                  languages=$prestashop_languages
                  current_lang=$prestashop_lang
                  key="PAYTY_ONEY_KEY"
                  option=$payty_default_oney_option
                  suffix=''
                }
              </script>

              <button type="button" id="payty_oney_options_btn"{if !empty($PAYTY_ONEY_OPTIONS)} style="display: none;"{/if} onclick="javascript: paytyAddOneyOption(true, '');">{l s='Add' mod='payty'}</button>

              <table id="payty_oney_options_table"{if empty($PAYTY_ONEY_OPTIONS)} style="display: none;"{/if} class="table" cellpadding="10" cellspacing="0">
                <thead>
                  <tr>
                    <th style="font-size: 10px;">{l s='Label' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Code' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Min amount' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Max amount' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Count' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Rate' mod='payty'}</th>
                    <th style="font-size: 10px;"></th>
                  </tr>
                </thead>

                <tbody>
                  {foreach from=$PAYTY_ONEY_OPTIONS key="key" item="option"}
                    {include file="./row_oney_option.tpl"
                      languages=$prestashop_languages
                      current_lang=$prestashop_lang
                      key=$key
                      option=$option
                      suffix=''
                    }
                  {/foreach}

                  <tr id="payty_oney_option_add">
                    <td colspan="6"></td>
                    <td>
                      <button type="button" onclick="javascript: paytyAddOneyOption(false, '');">{l s='Add' mod='payty'}</button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <p>
                {l s='Click on « Add » button to configure one or more payment options.' mod='payty'}<br />
                <b>{l s='Label' mod='payty'} : </b>{l s='The option label to display on the frontend (the %c and %r patterns will be respectively replaced by payments count and option rate).' mod='payty'}<br />
                <b>{l s='Code' mod='payty'} : </b>{l s='The option code as defined in your FacilyPay Oney contract.' mod='payty'}<br />
                <b>{l s='Min amount' mod='payty'} : </b>{l s='Minimum amount to enable the payment option.' mod='payty'}<br />
                <b>{l s='Max amount' mod='payty'} : </b>{l s='Maximum amount to enable the payment option.' mod='payty'}<br />
                <b>{l s='Count' mod='payty'} : </b>{l s='Total number of payments.' mod='payty'}<br />
                <b>{l s='Rate' mod='payty'} : </b>{l s='The interest rate in percentage.' mod='payty'}<br />
                <b>{l s='Do not forget to click on « Save » button to save your modifications.' mod='payty'}</b>
              </p>
            </div>
          </section>

          <script type="text/javascript">
            paytyOneyEnableOptionsChanged();
          </script>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['franfinance']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='FRANFINANCE PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_FFIN_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_FFIN_ENABLED" name="PAYTY_FFIN_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_FFIN_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_FFIN_TITLE"
              input_value=$PAYTY_FFIN_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          {if isset ($payty_countries_list['FFIN'])}
            <label for="PAYTY_FFIN_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_FFIN_COUNTRY" name="PAYTY_FFIN_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_FFIN_COUNTRY')">
                {foreach from=$payty_countries_options key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_FFIN_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
              <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
            </div>

            <div id="PAYTY_FFIN_COUNTRY_MENU" {if $PAYTY_FFIN_COUNTRY === '1'} style="display: none;"{/if}>
              <label for="PAYTY_FFIN_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
              <div class="margin-form">
                <select id="PAYTY_FFIN_COUNTRY_LST" name="PAYTY_FFIN_COUNTRY_LST[]" multiple="multiple" size="7">
                  {if isset ($payty_countries_list['FFIN'])}
                      {foreach from=$payty_countries_list['FFIN'] key="key" item="option"}
                          <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_FFIN_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                  {/if}
                </select>
              </div>
            </div>
          {else}
            <input type="hidden" name ="PAYTY_FFIN_COUNTRY" value="1">
            <input type="hidden" name ="PAYTY_FFIN_COUNTRY_LST[]" value ="">
            <p style="background: none repeat scroll 0 0 #FFFFE0; border: 1px solid #E6DB55; font-size: 13px; margin: 0 0 20px; padding: 10px;">
                {l s='Payment method unavailable for the list of countries defined on your PrestaShop store.' mod='payty'}
            </p>
          {/if}

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_FFIN_AMOUNTS"
              input_value=$PAYTY_FFIN_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

          <label>{l s='Payment options' mod='payty'}</label>
          <div class="margin-form">
            <script type="text/html" id="payty_ffin_row_option">
              {include file="./row_franfinance_option.tpl"
                languages=$prestashop_languages
                current_lang=$prestashop_lang
                key="PAYTY_FFIN_KEY"
                option=$payty_default_franfinance_option
              }
            </script>

            <button type="button" id="payty_ffin_options_btn"{if !empty($PAYTY_FFIN_OPTIONS)} style="display: none;"{/if} onclick="javascript: paytyAddFranfinanceOption(true, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>

            <table id="payty_ffin_options_table"{if empty($PAYTY_FFIN_OPTIONS)} style="display: none;"{/if} class="table" cellpadding="10" cellspacing="0">
              <thead>
                <tr>
                  <th style="font-size: 10px;">{l s='Label' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Count' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Fees' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Min amount' mod='payty'}</th>
                  <th style="font-size: 10px;">{l s='Max amount' mod='payty'}</th>
                  <th style="font-size: 10px;"></th>
                </tr>
              </thead>

              <tbody>
                {foreach from=$PAYTY_FFIN_OPTIONS key="key" item="option"}
                  {include file="./row_franfinance_option.tpl"
                    languages=$prestashop_languages
                    current_lang=$prestashop_lang
                    key=$key
                    option=$option
                  }
                {/foreach}

                <tr id="payty_ffin_option_add">
                  <td colspan="7"></td>
                  <td>
                    <button type="button" onclick="javascript: paytyAddFranfinanceOption(false, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>
                  </td>
                </tr>
              </tbody>
            </table>
            <p>
              {l s='Click on « Add » button to configure one or more payment options.' mod='payty'}<br />
              <b>{l s='Label' mod='payty'} : </b>{l s='The option label to display on the frontend (the %c pattern will be replaced by payments count).' mod='payty'}<br />
              <b>{l s='Count' mod='payty'} : </b>{l s='Total number of payments.' mod='payty'}<br />
              <b>{l s='Fees' mod='payty'} : </b>{l s='Enable or disables fees application.' mod='payty'}<br />
              <b>{l s='Min amount' mod='payty'} : </b>{l s='Minimum amount to enable the payment option.' mod='payty'}<br />
              <b>{l s='Max amount' mod='payty'} : </b>{l s='Maximum amount to enable the payment option.' mod='payty'}<br />
              <b>{l s='Do not forget to click on « Save » button to save your modifications.' mod='payty'}</b>
            </p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['fullcb']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='FULLCB PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_FULLCB_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_FULLCB_ENABLED" name="PAYTY_FULLCB_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_FULLCB_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_FULLCB_TITLE"
              input_value=$PAYTY_FULLCB_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          <div id="PAYTY_FULLCB_COUNTRY_MENU">
            <input type="hidden" name ="PAYTY_FULLCB_COUNTRY" value="1">
            <input type="hidden" name ="PAYTY_FULLCB_COUNTRY_LST[]" value ="FR">
            <label for="PAYTY_FULLCB_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
            <div class="margin-form">
              <span style="font-size: 13px; padding-top: 5px; vertical-align: middle;"><b>{$payty_countries_list['ps_countries']['FR']|escape:'html':'UTF-8'}</b></span>
            </div>
          </div>

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_FULLCB_AMOUNTS"
              input_value=$PAYTY_FULLCB_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_FULLCB_ENABLE_OPTS">{l s='Enable options selection' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_FULLCB_ENABLE_OPTS" name="PAYTY_FULLCB_ENABLE_OPTS" onchange="javascript: paytyFullcbEnableOptionsChanged();">
              {foreach from=$payty_yes_no_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_FULLCB_ENABLE_OPTS === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enable payment options selection on merchant site.' mod='payty'}</p>
          </div>

          <section id="payty_fullcb_options_settings">
            <label>{l s='Payment options' mod='payty'}</label>
            <div class="margin-form">
              <table class="table" cellpadding="10" cellspacing="0">
                <thead>
                  <tr>
                    <th style="font-size: 10px;">{l s='Activation' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Label' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Min amount' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Max amount' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Rate' mod='payty'}</th>
                    <th style="font-size: 10px;">{l s='Cap' mod='payty'}</th>
                  </tr>
                </thead>

                <tbody>
                  {foreach from=$PAYTY_FULLCB_OPTIONS key="key" item="option"}
                  <tr>
                    <td>
                      <input name="PAYTY_FULLCB_OPTIONS[{$key|escape:'html':'UTF-8'}][enabled]"
                        style="width: 100%;"
                        type="checkbox"
                        value="True"
                        {if !isset($option.enabled) || ($option.enabled === 'True')}checked{/if}>
                    </td>
                    <td>
                      {include file="./input_text_lang.tpl"
                        languages=$prestashop_languages
                        current_lang=$prestashop_lang
                        input_name="PAYTY_FULLCB_OPTIONS[{$key|escape:'html':'UTF-8'}][label]"
                        field_id="PAYTY_FULLCB_OPTIONS_{$key|escape:'html':'UTF-8'}_label"
                        input_value=$option['label']
                        style="width: 140px;"
                      }
                      <input name="PAYTY_FULLCB_OPTIONS[{$key|escape:'html':'UTF-8'}][count]" value="{$option['count']|escape:'html':'UTF-8'}" type="text" style="display: none; width: 0px;">
                    </td>
                    <td>
                      <input name="PAYTY_FULLCB_OPTIONS[{$key|escape:'html':'UTF-8'}][min_amount]"
                        value="{if isset($option)}{$option['min_amount']|escape:'html':'UTF-8'}{/if}"
                        style="width: 75px;"
                        type="text">
                    </td>
                    <td>
                      <input name="PAYTY_FULLCB_OPTIONS[{$key|escape:'html':'UTF-8'}][max_amount]"
                        value="{if isset($option)}{$option['max_amount']|escape:'html':'UTF-8'}{/if}"
                        style="width: 75px;"
                        type="text">
                    </td>
                    <td>
                      <input name="PAYTY_FULLCB_OPTIONS[{$key|escape:'html':'UTF-8'}][rate]"
                        value="{if isset($option)}{$option['rate']|escape:'html':'UTF-8'}{/if}"
                        style="width: 70px;"
                        type="text">
                    </td>
                    <td>
                      <input name="PAYTY_FULLCB_OPTIONS[{$key|escape:'html':'UTF-8'}][cap]"
                        value="{if isset($option)}{$option['cap']|escape:'html':'UTF-8'}{/if}"
                        style="width: 70px;"
                        type="text">
                    </td>
                  </tr>
                  {/foreach}
                </tbody>
              </table>
              <p>
                {l s='Configure FullCB payment options.' mod='payty'}<br />
                <b>{l s='Activation' mod='payty'} : </b>{l s='Enable / disable the payment option.' mod='payty'}<br />
                <b>{l s='Min amount' mod='payty'} : </b>{l s='Minimum amount to enable the payment option.' mod='payty'}<br />
                <b>{l s='Max amount' mod='payty'} : </b>{l s='Maximum amount to enable the payment option.' mod='payty'}<br />
                <b>{l s='Rate' mod='payty'} : </b>{l s='The interest rate in percentage.' mod='payty'}<br />
                <b>{l s='Cap' mod='payty'} : </b>{l s='Maximum fees amount of payment option.' mod='payty'}<br />
                <b>{l s='Do not forget to click on « Save » button to save your modifications.' mod='payty'}</b>
              </p>
            </div>
          </section>

          <script type="text/javascript">
            paytyFullcbEnableOptionsChanged();
          </script>
         </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['ancv']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='ANCV PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_ANCV_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ANCV_ENABLED" name="PAYTY_ANCV_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ANCV_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_ANCV_TITLE"
              input_value=$PAYTY_ANCV_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          <label for="PAYTY_ANCV_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ANCV_COUNTRY" name="PAYTY_ANCV_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_ANCV_COUNTRY')">
              {foreach from=$payty_countries_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ANCV_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
          </div>

          <div id="PAYTY_ANCV_COUNTRY_MENU" {if $PAYTY_ANCV_COUNTRY === '1'} style="display: none;"{/if}>
            <label for="PAYTY_ANCV_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_ANCV_COUNTRY_LST" name="PAYTY_ANCV_COUNTRY_LST[]" multiple="multiple" size="7">
                {foreach from=$payty_countries_list['ps_countries'] key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_ANCV_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
            </div>
          </div>

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_ANCV_AMOUNTS"
              input_value=$PAYTY_ANCV_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

          <label for="PAYTY_ANCV_DELAY">{l s='Capture delay' mod='payty'}</label>
          <div class="margin-form">
            <input id="PAYTY_ANCV_DELAY" name="PAYTY_ANCV_DELAY" value="{$PAYTY_ANCV_DELAY|escape:'html':'UTF-8'}" type="text">
            <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
          </div>

          <label for="PAYTY_ANCV_VALIDATION">{l s='Validation mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_ANCV_VALIDATION" name="PAYTY_ANCV_VALIDATION">
              <option value="-1"{if $PAYTY_ANCV_VALIDATION === '-1'} selected="selected"{/if}>{l s='Base settings configuration' mod='payty'}</option>
              {foreach from=$payty_validation_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_ANCV_VALIDATION === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['sepa']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='SEPA PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_SEPA_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_SEPA_ENABLED" name="PAYTY_SEPA_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SEPA_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_SEPA_TITLE"
              input_value=$PAYTY_SEPA_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          {if isset ($payty_countries_list['SEPA'])}
            <label for="PAYTY_SEPA_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_SEPA_COUNTRY" name="PAYTY_SEPA_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_SEPA_COUNTRY')">
                {foreach from=$payty_countries_options key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SEPA_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
              <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
            </div>

            <div id="PAYTY_SEPA_COUNTRY_MENU" {if $PAYTY_SEPA_COUNTRY === '1'} style="display: none;"{/if}>
              <label for="PAYTY_SEPA_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
              <div class="margin-form">
                <select id="PAYTY_SEPA_COUNTRY_LST" name="PAYTY_SEPA_COUNTRY_LST[]" multiple="multiple" size="7">
                  {if isset ($payty_countries_list['SEPA'])}
                      {foreach from=$payty_countries_list['SEPA'] key="key" item="option"}
                          <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_SEPA_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                  {/if}
                </select>
              </div>
            </div>
          {else}
            <input type="hidden" name ="PAYTY_SEPA_COUNTRY" value="1">
            <input type="hidden" name ="PAYTY_SEPA_COUNTRY_LST[]" value ="">
            <p style="background: none repeat scroll 0 0 #FFFFE0; border: 1px solid #E6DB55; font-size: 13px; margin: 0 0 20px; padding: 10px;">
                {l s='Payment method unavailable for the list of countries defined on your PrestaShop store.' mod='payty'}
            </p>
          {/if}

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_SEPA_AMOUNTS"
              input_value=$PAYTY_SEPA_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
         </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

          <label for="PAYTY_SEPA_DELAY">{l s='Capture delay' mod='payty'}</label>
          <div class="margin-form">
            <input id="PAYTY_SEPA_DELAY" name="PAYTY_SEPA_DELAY" value="{$PAYTY_SEPA_DELAY|escape:'html':'UTF-8'}" type="text">
            <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
          </div>

          <label for="PAYTY_SEPA_VALIDATION">{l s='Validation mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_SEPA_VALIDATION" name="PAYTY_SEPA_VALIDATION">
              <option value="-1"{if $PAYTY_SEPA_VALIDATION === '-1'} selected="selected"{/if}>{l s='Base settings configuration' mod='payty'}</option>
              {foreach from=$payty_validation_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SEPA_VALIDATION === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_SEPA_MANDATE_MODE">{l s='SEPA direct debit mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_SEPA_MANDATE_MODE" name="PAYTY_SEPA_MANDATE_MODE" onchange="javascript: paytySepa1clickPaymentMenuDisplay('PAYTY_SEPA_MANDATE_MODE')">
              {foreach from=$payty_sepa_mandate_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SEPA_MANDATE_MODE === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Select SEPA direct debit mode. Attention, the two last choices require the payment by token option on %s.' sprintf='Payty' mod='payty'}</p>
          </div>

          <div id="PAYTY_SEPA_1_CLICK_PAYMNT_MENU"  {if $PAYTY_SEPA_MANDATE_MODE !== 'REGISTER_PAY'} style="display: none;"{/if}>
            <label for="PAYTY_SEPA_1_CLICK_PAYMNT">{l s='Payment by token' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_SEPA_1_CLICK_PAYMNT" name="PAYTY_SEPA_1_CLICK_PAYMNT">
                {foreach from=$payty_yes_no_options key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SEPA_1_CLICK_PAYMNT === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
              <p>{l s='The payment by token allows to pay orders without re-entering bank data at each payment. The "payment by token" option should be enabled on your %s store to use this feature.' sprintf='Payty' mod='payty'}</p>
            </div>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['paypal']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='PAYPAL PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_PAYPAL_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_PAYPAL_ENABLED" name="PAYTY_PAYPAL_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_PAYPAL_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_PAYPAL_TITLE"
              input_value=$PAYTY_PAYPAL_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          <label for="PAYTY_PAYPAL_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_PAYPAL_COUNTRY" name="PAYTY_PAYPAL_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_PAYPAL_COUNTRY')">
              {foreach from=$payty_countries_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_PAYPAL_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
          </div>

          <div id="PAYTY_PAYPAL_COUNTRY_MENU" {if $PAYTY_PAYPAL_COUNTRY === '1'} style="display: none;"{/if}>
            <label for="PAYTY_PAYPAL_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_PAYPAL_COUNTRY_LST" name="PAYTY_PAYPAL_COUNTRY_LST[]" multiple="multiple" size="7">
                {foreach from=$payty_countries_list['ps_countries'] key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_PAYPAL_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
            </div>
          </div>

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_PAYPAL_AMOUNTS"
              input_value=$PAYTY_PAYPAL_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='PAYMENT PAGE' mod='payty'}</legend>

          <label for="PAYTY_PAYPAL_DELAY">{l s='Capture delay' mod='payty'}</label>
          <div class="margin-form">
            <input id="PAYTY_PAYPAL_DELAY" name="PAYTY_PAYPAL_DELAY" value="{$PAYTY_PAYPAL_DELAY|escape:'html':'UTF-8'}" type="text">
            <p>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}</p>
          </div>

          <label for="PAYTY_PAYPAL_VALIDATION">{l s='Validation mode' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_PAYPAL_VALIDATION" name="PAYTY_PAYPAL_VALIDATION">
              <option value="-1"{if $PAYTY_PAYPAL_VALIDATION === '-1'} selected="selected"{/if}>{l s='Base settings configuration' mod='payty'}</option>
              {foreach from=$payty_validation_mode_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_PAYPAL_VALIDATION === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    {if $payty_plugin_features['sofort']}
      <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
        <a href="#">{l s='SOFORT BANKING PAYMENT' mod='payty'}</a>
      </h4>
      <div>
        <fieldset>
          <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

          <label for="PAYTY_SOFORT_ENABLED">{l s='Activation' mod='payty'}</label>
          <div class="margin-form">
            <select id="PAYTY_SOFORT_ENABLED" name="PAYTY_SOFORT_ENABLED">
              {foreach from=$payty_enable_disable_options key="key" item="option"}
                <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SOFORT_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
              {/foreach}
            </select>
            <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
          </div>

          <label>{l s='Payment method title' mod='payty'}</label>
          <div class="margin-form">
            {include file="./input_text_lang.tpl"
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              input_name="PAYTY_SOFORT_TITLE"
              input_value=$PAYTY_SOFORT_TITLE
              style="width: 330px;"
            }
            <p>{l s='Method title to display on payment means page.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>

        <fieldset>
          <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

          {if isset ($payty_countries_list['SOFORT'])}
            <label for="PAYTY_SOFORT_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
            <div class="margin-form">
              <select id="PAYTY_SOFORT_COUNTRY" name="PAYTY_SOFORT_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_SOFORT_COUNTRY')">
                {foreach from=$payty_countries_options key="key" item="option"}
                  <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_SOFORT_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                {/foreach}
              </select>
              <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
            </div>

            <div id="PAYTY_SOFORT_COUNTRY_MENU" {if $PAYTY_SOFORT_COUNTRY === '1'} style="display: none;"{/if}>
              <label for="PAYTY_SOFORT_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
              <div class="margin-form">
                <select id="PAYTY_SOFORT_COUNTRY_LST" name="PAYTY_SOFORT_COUNTRY_LST[]" multiple="multiple" size="7">
                  {if isset ($payty_countries_list['SOFORT'])}
                      {foreach from=$payty_countries_list['SOFORT'] key="key" item="option"}
                          <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_SOFORT_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
                      {/foreach}
                  {/if}
                </select>
              </div>
            </div>
          {else}
            <input type="hidden" name ="PAYTY_SOFORT_COUNTRY" value="1">
            <input type="hidden" name ="PAYTY_SOFORT_COUNTRY_LST[]" value ="">
            <p style="background: none repeat scroll 0 0 #FFFFE0; border: 1px solid #E6DB55; font-size: 13px; margin: 0 0 20px; padding: 10px;">
                {l s='Payment method unavailable for the list of countries defined on your PrestaShop store.' mod='payty'}
            </p>
          {/if}

          <label>{l s='Customer group amount restriction' mod='payty'}</label>
          <div class="margin-form">
            {include file="./table_amount_group.tpl"
              groups=$prestashop_groups
              input_name="PAYTY_SOFORT_AMOUNTS"
              input_value=$PAYTY_SOFORT_AMOUNTS
            }
            <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
          </div>
        </fieldset>
        <div class="clear">&nbsp;</div>
      </div>
    {/if}

    <h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
      <a href="#">{l s='OTHER PAYMENT MEANS' mod='payty'}</a>
    </h4>
    <div>
      <fieldset>
        <legend>{l s='MODULE OPTIONS' mod='payty'}</legend>

        <label for="PAYTY_OTHER_ENABLED">{l s='Activation' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_OTHER_ENABLED" name="PAYTY_OTHER_ENABLED">
            {foreach from=$payty_enable_disable_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_OTHER_ENABLED === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='Enables / disables this payment method.' mod='payty'}</p>
        </div>

        <label>{l s='Payment method title' mod='payty'}</label>
        <div class="margin-form">
          {include file="./input_text_lang.tpl"
            languages=$prestashop_languages
            current_lang=$prestashop_lang
            input_name="PAYTY_OTHER_TITLE"
            input_value=$PAYTY_OTHER_TITLE
            style="width: 330px;"
          }
          <p>{l s='Method title to display on payment means page. Used only if « Regroup payment means » option is enabled.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>

      <fieldset>
        <legend>{l s='RESTRICTIONS' mod='payty'}</legend>

        <label for="PAYTY_OTHER_COUNTRY">{l s='Restrict to some countries' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_OTHER_COUNTRY" name="PAYTY_OTHER_COUNTRY" onchange="javascript: paytyCountriesRestrictMenuDisplay('PAYTY_OTHER_COUNTRY')">
            {foreach from=$payty_countries_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_OTHER_COUNTRY === (string)$key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='Buyer\'s billing countries in which this payment method is available.' mod='payty'}</p>
        </div>

        <div id="PAYTY_OTHER_COUNTRY_MENU" {if $PAYTY_OTHER_COUNTRY === '1'} style="display: none;"{/if}>
        <label for="PAYTY_OTHER_COUNTRY_LST">{l s='Authorized countries' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_OTHER_COUNTRY_LST" name="PAYTY_OTHER_COUNTRY_LST[]" multiple="multiple" size="7">
            {foreach from=$payty_countries_list['ps_countries'] key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if in_array($key, $PAYTY_OTHER_COUNTRY_LST)} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
        </div>
        </div>

        <label>{l s='Customer group amount restriction' mod='payty'}</label>
        <div class="margin-form">
          {include file="./table_amount_group.tpl"
            groups=$prestashop_groups
            input_name="PAYTY_OTHER_AMOUNTS"
            input_value=$PAYTY_OTHER_AMOUNTS
          }
          <p>{l s='Define amount restriction for each customer group.' mod='payty'}</p>
        </div>
      </fieldset>
      <div class="clear payty-grouped">&nbsp;</div>

      <fieldset>
        <legend>{l s='PAYMENT OPTIONS' mod='payty'}</legend>

        <label for="PAYTY_OTHER_GROUPED_VIEW">{l s='Regroup payment means ' mod='payty'}</label>
        <div class="margin-form">
          <select id="PAYTY_OTHER_GROUPED_VIEW" name="PAYTY_OTHER_GROUPED_VIEW" onchange="javascript: paytyGroupedViewChanged();">
            {foreach from=$payty_enable_disable_options key="key" item="option"}
              <option value="{$key|escape:'html':'UTF-8'}"{if $PAYTY_OTHER_GROUPED_VIEW === $key} selected="selected"{/if}>{$option|escape:'html':'UTF-8'}</option>
            {/foreach}
          </select>
          <p>{l s='If this option is enabled, all the payment means added in this section will be displayed within the same payment submodule.' mod='payty'}</p>
        </div>

        <label>{l s='Payment means' mod='payty'}</label>
        <div class="margin-form">
          {assign var=merged_array_cards value=$payty_payment_cards_options}
          {assign var=VALID_PAYTY_EXTRA_PAYMENT_MEANS value=[]}
          {foreach from=$PAYTY_EXTRA_PAYMENT_MEANS key="key_card" item="option_card"}
              {if ! isset($merged_array_cards[$option_card.code])}
                  {append var='merged_array_cards' value=$option_card.title index=$option_card.code}
                  {$VALID_PAYTY_EXTRA_PAYMENT_MEANS.$key_card = $option_card}
              {/if}
          {/foreach}

          <script type="text/html" id="payty_other_payment_means_row_option">
            {include file="./row_other_payment_means_option.tpl"
              payment_means_cards=$merged_array_cards
              countries_list=$payty_countries_list['ps_countries']
              validation_mode_options=$payty_validation_mode_options
              enable_disable_options=$payty_enable_disable_options
              languages=$prestashop_languages
              current_lang=$prestashop_lang
              key="PAYTY_OTHER_PAYMENT_SCRIPT_MEANS_KEY"
              option=$payty_default_other_payment_means_option
            }
          </script>

          <button type="button" id="payty_other_payment_means_options_btn"{if !empty($PAYTY_OTHER_PAYMENT_MEANS)} style="display: none;"{/if} onclick="javascript: paytyAddOtherPaymentMeansOption(true, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>

          <table id="payty_other_payment_means_options_table"{if empty($PAYTY_OTHER_PAYMENT_MEANS)} style="display: none;"{/if} class="table" cellpadding="10" cellspacing="0">
          <thead>
            <tr>
              <th style="font-size: 10px;">{l s='Label' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Means of payment' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Countries' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Min amount' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Max amount' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Capture' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Validation mode' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Cart data' mod='payty'}</th>
              <th style="font-size: 10px;"></th>
            </tr>
          </thead>

          <tbody>
            {foreach from=$PAYTY_OTHER_PAYMENT_MEANS key="key" item="option"}
              {include file="./row_other_payment_means_option.tpl"
                payment_means_cards=$merged_array_cards
                countries_list=$payty_countries_list['ps_countries']
                validation_mode_options=$payty_validation_mode_options
                enable_disable_options=$payty_enable_disable_options
                languages=$prestashop_languages
                current_lang=$prestashop_lang
                key=$key
                option=$option
              }
            {/foreach}

            <tr id="payty_other_payment_means_option_add">
              <td colspan="8"></td>
              <td>
                <button type="button" onclick="javascript: paytyAddOtherPaymentMeansOption(false, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>
              </td>
            </tr>
          </tbody>
          </table>

          {if empty($PAYTY_OTHER_PAYMENT_MEANS)}
            <input type="hidden" id="PAYTY_OTHER_PAYMENT_MEANS" name="PAYTY_OTHER_PAYMENT_MEANS" value="">
          {/if}

          <p>
            {l s='Click on « Add » button to configure one or more payment means.' mod='payty'}<br />
            <b>{l s='Label' mod='payty'} : </b>{l s='The label of the means of payment to display on your site.' mod='payty'}<br />
            <b>{l s='Means of payment' mod='payty'} : </b>{l s='Choose the means of payment you want to propose.' mod='payty'}<br />
            <b>{l s='Countries' mod='payty'} : </b>{l s='Countries where the means of payment will be available. Keep blank to authorize all countries.' mod='payty'}<br />
            <b>{l s='Min amount' mod='payty'} : </b>{l s='Minimum amount to enable the means of payment.' mod='payty'}<br />
            <b>{l s='Max amount' mod='payty'} : </b>{l s='Maximum amount to enable the means of payment.' mod='payty'}<br />
            <b>{l s='Capture' mod='payty'} : </b>{l s='The number of days before the bank capture. Enter value only if different from « Base settings ».' mod='payty'}<br />
            <b>{l s='Validation mode' mod='payty'} : </b>{l s='If manual is selected, you will have to confirm payments manually in your bank Back Office.' mod='payty'}<br />
            <b>{l s='Cart data' mod='payty'} : </b>{l s='If you disable this option, the shopping cart details will not be sent to the gateway. Attention, in some cases, this option has to be enabled. For more information, refer to the module documentation.' mod='payty'}<br />
            <b>{l s='Do not forget to click on « Save » button to save your modifications.' mod='payty'}</b>
          </p>
        </div>

        <label>{l s='Add payment means' mod='payty'}</label>
        <div class="margin-form">
          <script type="text/html" id="payty_add_payment_means_row_option">
            {include file="./row_extra_means_of_payment.tpl"
              key="PAYTY_EXTRA_PAYMENT_MEANS_SCRIPT_KEY"
              option=$payty_default_extra_payment_means_option
            }
          </script>

          <button type="button" id="payty_extra_payment_means_options_btn"{if !empty($VALID_PAYTY_EXTRA_PAYMENT_MEANS)} style="display: none;"{/if} onclick="javascript: paytyAddExtraPaymentMeansOption(true, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>

          <table id="payty_extra_payment_means_options_table"{if empty($VALID_PAYTY_EXTRA_PAYMENT_MEANS)} style="display: none;"{/if} class="table" cellpadding="10" cellspacing="0">
          <thead>
            <tr>
              <th style="font-size: 10px;">{l s='Code' mod='payty'}</th>
              <th style="font-size: 10px; width: 350px;">{l s='Label' mod='payty'}</th>
              <th style="font-size: 10px;">{l s='Action' mod='payty'}</th>
            </tr>
          </thead>

          <tbody>
            {foreach from=$VALID_PAYTY_EXTRA_PAYMENT_MEANS key="key" item="option"}
                {include file="./row_extra_means_of_payment.tpl"
                    key=$key
                    option=$option
                }
            {/foreach}

            <tr id="payty_extra_payment_means_option_add">
              <td colspan="2"></td>
              <td>
                <button type="button" onclick="javascript: paytyAddExtraPaymentMeansOption(false, '{l s='Delete' mod='payty'}');">{l s='Add' mod='payty'}</button>
              </td>
            </tr>
          </tbody>
          </table>

          {if empty($VALID_PAYTY_EXTRA_PAYMENT_MEANS)}
            <input type="hidden" id="PAYTY_EXTRA_PAYMENT_MEANS" name="PAYTY_EXTRA_PAYMENT_MEANS" value="">
          {/if}

          <p>
            {l s='Click on « Add » button to add one or more new payment means.' mod='payty'}<br />
            <b>{l s='Code' mod='payty'} : </b>{l s='The code of the means of payment as expected by Payty gateway.' sprintf='Payty' mod='payty'}<br />
            <b>{l s='Label' mod='payty'} : </b>{l s='The default label of the means of payment.' mod='payty'}<br />
            <b>{l s='Do not forget to click on « Save » button to save your modifications.' mod='payty'}</b>
          </p>
        </div>
      </fieldset>
      <div class="clear">&nbsp;</div>
    </div>

   </div>

  {if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
    <div class="clear" style="width: 100%;">
      <input type="submit" class="button" name="payty_submit_admin_form" value="{l s='Save' mod='payty'}" style="float: right;">
    </div>
  {else}
    <div class="panel-footer" style="width: 100%;">
      <button type="submit" value="1" name="payty_submit_admin_form" class="btn btn-default pull-right" style="float: right !important;">
        <i class="process-icon-save"></i>
        {l s='Save' mod='payty'}
      </button>
    </div>
  {/if}
</form>

<br />
<br />