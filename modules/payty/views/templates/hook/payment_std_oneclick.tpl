{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<!-- This meta tag is mandatory to avoid encoding problems caused by \PrestaShop\PrestaShop\Core\Payment\PaymentOptionFormDecorator -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<section style="margin-bottom: 2rem;">
<div id="payty_oneclick_payment_description">
  <ul id="payty_oneclick_payment_description_1">
    <li>
      <span>{l s='You will pay with your registered means of payment' mod='payty'}<b> {$payty_saved_payment_mean|escape:'html':'UTF-8'}. </b>{l s='No data entry is needed.' mod='payty'}</span>
    </li>

    <li style="margin: 8px 0px 8px;">
      <span>{l s='OR' mod='payty'}</span>
    </li>

    <li>
      <a href="javascript: void(0);" onclick="paytyOneclickPaymentSelect(0)">{l s='Click here to pay with another means of payment.' mod='payty'}</a>
    </li>
  </ul>
{if ($payty_std_card_data_mode == '2')}
  </div>
    <script type="text/javascript">
      function paytyOneclickPaymentSelect(paymentByIdentifier) {
        if (paymentByIdentifier) {
          $('#payty_oneclick_payment_description_1').show();
          $('#payty_standard').hide();
          $('#payty_payment_by_identifier').val('1');
        } else {
          $('#payty_oneclick_payment_description_1').hide();
          $('#payty_standard').show();
          $('#payty_payment_by_identifier').val('0');
         }
       }
     </script>
{else}
    <ul id="payty_oneclick_payment_description_2" style="display: none;">
      {if ($payty_std_card_data_mode != '5') || $payty_std_card_data_mode == '6'}
        <li>{l s='You will enter payment data after order confirmation.' mod='payty'}</li>
      {/if}

      <li style="margin: 8px 0px 8px;">
        <span>{l s='OR' mod='payty'}</span>
      </li>
      <li>
        <a href="javascript: void(0);" onclick="paytyOneclickPaymentSelect(1)">{l s='Click here to pay with your registered means of payment.' mod='payty'}</a>
      </li>
    </ul>
  </div>

  <script type="text/javascript">
    function paytyOneclickPaymentSelect(paymentByIdentifier) {
      if (paymentByIdentifier) {
        $('#payty_oneclick_payment_description_1').show();
        $('#payty_oneclick_payment_description_2').hide()
        $('#payty_payment_by_identifier').val('1');
      } else {
        $('#payty_oneclick_payment_description_1').hide();
        $('#payty_oneclick_payment_description_2').show();
        $('#payty_payment_by_identifier').val('0');
      }

      {if ($payty_std_card_data_mode == '5' || $payty_std_card_data_mode == '6')}
        $('.payty .kr-form-error').html('');

        var token;
        if ($('#payty_payment_by_identifier').val() == '1') {
          token = "{$payty_rest_identifier_token|escape:'html':'UTF-8'}";
        } else {
          token = "{$payty_rest_form_token|escape:'html':'UTF-8'}";
        }

        KR.setFormConfig({ formToken: token, language: PAYTY_LANGUAGE });
      {/if}
    }
  </script>
{/if}
</section>

<script type="text/javascript">
  window.onload = function() {
      $("input[data-module-name=payty]").change(function() {
        if ($(this).is(':checked')) {
          paytyOneclickPaymentSelect(1);
          if (typeof paytySepaOneclickPaymentSelect == 'function') {
            paytySepaOneclickPaymentSelect(1);
          }
        }
      });
  };
</script>