{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<!-- This meta tag is mandatory to avoid encoding problems caused by \PrestaShop\PrestaShop\Core\Payment\PaymentOptionFormDecorator -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<section style="margin-top: -12px;">
  <iframe class="payty-iframe" id="payty_iframe" src="{$link->getModuleLink('payty', 'iframe', array(), true)|escape:'html':'UTF-8'}" style="display: none;">
  </iframe>

   {if $payty_can_cancel_iframe}
       <a id="payty_cancel_iframe" class="payty-iframe" style="margin-bottom: 8px; display: none;" href="javascript:paytyInit();">
           {l s='< Cancel and return to payment choice' mod='payty'}
       </a>
   {/if}
</section>
<br />

<script type="text/javascript">
  var paytySubmit = function(e) {
    e.preventDefault();

    if (!$('#payty_standard').data('submitted')) {
      $('#payty_standard').data('submitted', true);
      $('#payment-confirmation button').attr('disabled', 'disabled');
      $('.payty-iframe').show();
      $('#payty_oneclick_payment_description').hide();

      var url = decodeURIComponent("{$link->getModuleLink('payty', 'redirect', ['content_only' => 1], true)|escape:'url':'UTF-8'}") + '&' + Date.now();
      {if $payty_is_valid_std_identifier}
        url = url + '&payty_payment_by_identifier=' + $('#payty_payment_by_identifier').val();
      {/if}

      $('#payty_iframe').attr('src', url);
    }

    return false;
  }

  setTimeout(function() {
    $('input[type="radio"][name="payment-option"]').change(function() {
      paytyInit();
    });
  }, 0);

  function paytyInit() {
    if (!$('#payty_standard').data('submitted')) {
      return;
    }

    $('#payty_standard').data('submitted', false);
    $('#payment-confirmation button').removeAttr('disabled');
    $('.payty-iframe').hide();
    $('#payty_oneclick_payment_description').show();

    var url = decodeURIComponent("{$link->getModuleLink('payty', 'iframe', ['content_only' => 1], true)|escape:'url':'UTF-8'}") + '&' + Date.now();
    $('#payty_iframe').attr('src', url);
  }
</script>