{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<div style="padding-left: 40px;" id="payty_oneclick_payment_description">
  <ul id="payty_oneclick_payment_description_1">
    <li>
      <span class="payty_span">{l s='You will pay with your registered means of payment' mod='payty'}<b> {$payty_saved_payment_mean|escape:'html':'UTF-8'}. </b>{l s='No data entry is needed.' mod='payty'}</span>
    </li>

    <li style="margin: 8px 0px 8px;">
      <span class="payty_span">{l s='OR' mod='payty'}</span>
    </li>

    <li>
      <p class="payty_link" onclick="paytyOneclickPaymentSelect(0)">{l s='Click here to pay with another means of payment.' mod='payty'}</p>
    </li>
  </ul>
{if ($payty_std_card_data_mode == '2')}
  <script type="text/javascript">
    function paytyOneclickPaymentSelect(paymentByIdentifier) {
      if (paymentByIdentifier) {
        $('#payty_oneclick_payment_description').show();
        $('#payty_standard').hide();
        $('#payty_payment_by_identifier').val('1');
      } else {
        $('#payty_oneclick_payment_description').hide();
        $('#payty_standard').show();
        $('#payty_payment_by_identifier').val('0');
      }
    }
  </script>
{else}
  <ul id="payty_oneclick_payment_description_2" style="display: none;">
    {if ($payty_std_card_data_mode != '5') || ($payty_std_card_data_mode == '6')}
      <li>{l s='You will enter payment data after order confirmation.' mod='payty'}</li>
    {/if}

      <li style="margin: 8px 0px 8px;">
        <span class="payty_span">{l s='OR' mod='payty'}</span>
      </li>
      <li>
        <p class="payty_link" onclick="paytyOneclickPaymentSelect(1)">{l s='Click here to pay with your registered means of payment.' mod='payty'}</p>
      </li>
  </ul>

  <script type="text/javascript">
    $(document).ready(function() {
       sessionStorage.setItem('paytyIdentifierToken', "{$payty_rest_identifier_token|escape:'html':'UTF-8'}");
       sessionStorage.setItem('paytyToken', "{$payty_rest_form_token|escape:'html':'UTF-8'}");
    });

    function paytyOneclickPaymentSelect(paymentByIdentifier) {
      if (paymentByIdentifier) {
        $('#payty_oneclick_payment_description_1').show();
        $('#payty_oneclick_payment_description_2').hide()
        $('#payty_payment_by_identifier').val('1');
      } else {
        $('#payty_oneclick_payment_description_1').hide();
        $('#payty_oneclick_payment_description_2').show();
        $('#payty_payment_by_identifier').val('0');
      }

      {if ($payty_std_card_data_mode == '5' || $payty_std_card_data_mode == '6')}
        $('.payty .kr-form-error').html('');

        var token;
        if ($('#payty_payment_by_identifier').val() == '1') {
          token = sessionStorage.getItem('paytyIdentifierToken');
        } else {
          token = sessionStorage.getItem('paytyToken');
        }

        KR.setFormConfig({ formToken: token, language: PAYTY_LANGUAGE });
      {/if}
    }
    </script>
{/if}

{if ($payty_std_card_data_mode != '5' && $payty_std_card_data_mode != '6')}
    {if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
      <input id="payty_standard_link" value="{l s='Pay' mod='payty'}" class="button" />
    {else}
      <button id="payty_standard_link" class="button btn btn-default standard-checkout button-medium">
        <span>{l s='Pay' mod='payty'}</span>
      </button>
    {/if}
{/if}
</div>