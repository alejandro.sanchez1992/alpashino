{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
<div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}

<div class="payment_module payty {$payty_tag|escape:'html':'UTF-8'}">
{if $payty_is_valid_std_identifier}
  <a class="unclickable payty-standard-link" title="{l s='Choose pay with registred means of payment or enter payment information and click « Pay » button' mod='payty'}">
    <img class="logo" src="{$payty_logo|escape:'html':'UTF-8'}" />{$payty_title|escape:'html':'UTF-8'}
{else}
  <a href="javascript: void(0);" title="{l s='Click here to pay by credit card' mod='payty'}" id="payty_standard_link" class="payty-standard-link">
    <img class="logo" src="{$payty_logo|escape:'html':'UTF-8'}" />{$payty_title|escape:'html':'UTF-8'}
    <br />
{/if}

    {if $payty_is_valid_std_identifier}
      <br /><br />
      {include file="./payment_std_oneclick.tpl"}
      <input id="payty_payment_by_identifier" type="hidden" name="payty_payment_by_identifier" value="1" />
    {/if}

    <iframe class="payty-iframe" id="payty_iframe" src="{$link->getModuleLink('payty', 'iframe', ['content_only' => 1], true)|escape:'html':'UTF-8'}" style="display: none;">
    </iframe>

    {if $payty_can_cancel_iframe}
        <button class="payty-iframe" id="payty_cancel_iframe" style="display: none;">{l s='< Cancel and return to payment choice' mod='payty'}</button>
    {/if}
  </a>

  <script type="text/javascript">
    var done = false;
    function paytyShowIframe() {
      if (done) {
        return;
      }

      done = true;

      {if !$payty_is_valid_std_identifier}
        $('#payty_iframe').parent().addClass('unclickable');
      {/if}

      $('.payty-iframe').show();
      $('#payty_oneclick_payment_description').hide();

      var url = "{$link->getModuleLink('payty', 'redirect', ['content_only' => 1], true)|escape:'url':'UTF-8'}";
      {if $payty_is_valid_std_identifier}
            url = url + '&payty_payment_by_identifier=' + $('#payty_payment_by_identifier').val();
      {/if}

      $('#payty_iframe').attr('src', decodeURIComponent(url) + '&' + Date.now());
    }

    function paytyHideIframe() {
      if (!done) {
        return;
      }

      done = false;

      {if !$payty_is_valid_std_identifier}
        $('#payty_iframe').parent().removeClass('unclickable');
      {/if}

      $('.payty-iframe').hide();
      $('#payty_oneclick_payment_description').show();

      var url = "{$link->getModuleLink('payty', 'iframe', ['content_only' => 1], true)|escape:'url':'UTF-8'}";
      $('#payty_iframe').attr('src', decodeURIComponent(url) + '&' + Date.now());
    }

    $(function() {
      $('#payty_standard_link').click(paytyShowIframe);
      $('#payty_cancel_iframe').click(function() {
        paytyHideIframe();
        return false;
      });

      $('.payment_module a:not(.payty-standard-link)').click(paytyHideIframe);
    });
  </script>
</div>

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
</div></div>
{/if}