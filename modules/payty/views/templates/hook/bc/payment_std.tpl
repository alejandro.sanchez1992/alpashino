{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
  <div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}

<div class="payment_module payty {$payty_tag|escape:'html':'UTF-8'}">
  {if $payty_std_card_data_mode == 1 && !$payty_is_valid_std_identifier}
    <a href="javascript: $('#payty_standard').submit();" title="{l s='Click here to pay by credit card' mod='payty'}">
  {else}
    <a class="unclickable"
      {if $payty_is_valid_std_identifier}
        title="{l s='Choose pay with registred means of payment or enter payment information and click « Pay » button' mod='payty'}"
      {else}
        title="{l s='Enter payment information and click « Pay » button' mod='payty'}"
      {/if}
    >
  {/if}
    <img class="logo" src="{$payty_logo|escape:'html':'UTF-8'}" />{$payty_title|escape:'html':'UTF-8'}
    {if $payty_is_valid_std_identifier}
      <br /><br />
      {include file="./payment_std_oneclick.tpl"}
    {/if}

    <form action="{$link->getModuleLink('payty', 'redirect', array(), true)|escape:'html':'UTF-8'}"
          method="post" id="payty_standard"
          {if $payty_is_valid_std_identifier} style="display: none;"{/if}
    >

      <input type="hidden" name="payty_payment_type" value="standard" />

      {if $payty_is_valid_std_identifier}
        <input id="payty_payment_by_identifier" type="hidden" name="payty_payment_by_identifier" value="1" />
      {/if}

      {if ($payty_std_card_data_mode == 2)}
        <br />

        {assign var=first value=true}
        {foreach from=$payty_avail_cards key="key" item="card"}
          <div class="payty-pm">
            {if $payty_avail_cards|@count == 1}
              <input type="hidden" id="payty_card_type_{$key|escape:'html':'UTF-8'}" name="payty_card_type" value="{$key|escape:'html':'UTF-8'}" >
            {else}
              <input type="radio" id="payty_card_type_{$key|escape:'html':'UTF-8'}" name="payty_card_type" value="{$key|escape:'html':'UTF-8'}" style="vertical-align: middle;"{if $first == true} checked="checked"{/if} >
            {/if}

            <label for="payty_card_type_{$key|escape:'html':'UTF-8'}">
              <img src="{$card['logo']}" alt="{$card['label']|escape:'html':'UTF-8'}" title="{$card['label']|escape:'html':'UTF-8'}" >
            </label>

            {assign var=first value=false}
          </div>
        {/foreach}
        <br />
        <div style="margin-bottom: 12px;"></div>

        {if $payty_is_valid_std_identifier}
            <div>
                <ul>
                    {if $payty_std_card_data_mode == 2}
                        <li>
                            <span class="payty_span">{l s='You will enter payment data after order confirmation.' mod='payty'}</span>
                        </li>
                    {/if}
                    <li style="margin: 8px 0px 8px;">
                        <span class="payty_span">{l s='OR' mod='payty'}</span>
                    </li>
                    <li>
                        <p class="payty_link" onclick="paytyOneclickPaymentSelect(1)">{l s='Click here to pay with your registered means of payment.' mod='payty'}</p>
                    </li>
                </ul>
            </div>
        {/if}

        {if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
          <input id="payty_submit_form" type="submit" name="submit" value="{l s='Pay' mod='payty'}" class="button"/>
        {else}
          <button id="payty_submit_form" type="submit" name="submit" class="button btn btn-default standard-checkout button-medium">
            <span>{l s='Pay' mod='payty'}</span>
          </button>
        {/if}
      {/if}
    </form>

    {if $payty_is_valid_std_identifier}
      <script type="text/javascript">
        $('#payty_standard_link').click(function(){
          {if ($payty_std_card_data_mode == 2)}
            $('#payty_submit_form').click();
          {else}
            $('#payty_standard').submit();
          {/if}
        });
      </script>
    {/if}
  </a>
</div>

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
  </div></div>
{/if}