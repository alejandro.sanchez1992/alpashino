{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<div style="padding-left: 40px;" id="payty_sepa_oneclick_payment_description">
  <ul id="payty_sepa_oneclick_payment_description_1">
    <li>
      <span class="payty_span">{l s='You will pay with your registered means of payment' mod='payty'}<b> {$payty_sepa_saved_payment_mean|escape:'html':'UTF-8'}. </b>{l s='No data entry is needed.' mod='payty'}</span>
    </li>

    <li style="margin: 8px 0px 8px;">
      <span class="payty_span">{l s='OR' mod='payty'}</span>
    </li>

    <li>
      <p class="payty_link" onclick="paytySepaOneclickPaymentSelect(0)">{l s='Click here to pay with another means of payment.' mod='payty'}</p>
    </li>
  </ul>
  <ul id="payty_sepa_oneclick_payment_description_2" style="display: none;">
    <li>{l s='You will enter payment data after order confirmation.' mod='payty'}</li>
    <li style="margin: 8px 0px 8px;">
      <span class="payty_span">{l s='OR' mod='payty'}</span>
    </li>
    <li>
      <p class="payty_link" onclick="paytySepaOneclickPaymentSelect(1)">{l s='Click here to pay with your registered means of payment.' mod='payty'}</p>
    </li>
  </ul>

  <script type="text/javascript">
    function paytySepaOneclickPaymentSelect(paymentByIdentifier) {
      if (paymentByIdentifier) {
        $('#payty_sepa_oneclick_payment_description_1').show();
        $('#payty_sepa_oneclick_payment_description_2').hide()
        $('#payty_sepa_payment_by_identifier').val('1');
      } else {
        $('#payty_sepa_oneclick_payment_description_1').hide();
        $('#payty_sepa_oneclick_payment_description_2').show();
        $('#payty_sepa_payment_by_identifier').val('0');
      }
    }
  </script>
</div>