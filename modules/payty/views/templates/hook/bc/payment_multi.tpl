{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
  <div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}

{if {$payty_multi_options|@count} == 1 AND ($payty_multi_card_mode == 1)}
  <div class="payment_module payty {$payty_tag|escape:'html':'UTF-8'} multi">
    {foreach from=$payty_multi_options key="key" item="option"}
      <a href="javascript: $('#payty_opt').val('{$key|escape:'html':'UTF-8'}'); $('#payty_multi').submit();"
         title="{l s='Click to pay in installments' mod='payty'}">

        <img class="logo" src="{$payty_logo|escape:'html':'UTF-8'}" />{$payty_title|escape:'html':'UTF-8'}
        ({$option.localized_label|escape:'html':'UTF-8'})

        <form action="{$link->getModuleLink('payty', 'redirect', array(), true)|escape:'html':'UTF-8'}" method="post" id="payty_multi">
          <input type="hidden" name="payty_payment_type" value="multi" />
          <input type="hidden" name="payty_opt" value="" id="payty_opt" />
        </form>
      </a>
    {/foreach}
  </div>
{else}
  <div class="payment_module payty {$payty_tag|escape:'html':'UTF-8'} multi">
    <a class="unclickable" title="{l s='Click on a payment option to pay in installments' mod='payty'}" href="javascript: void(0);">
      <img class="logo" src="{$payty_logo|escape:'html':'UTF-8'}" />{$payty_title|escape:'html':'UTF-8'}

      <form action="{$link->getModuleLink('payty', 'redirect', array(), true)|escape:'html':'UTF-8'}" method="post" id="payty_multi">
        <input type="hidden" name="payty_payment_type" value="multi" />
        <input type="hidden" name="payty_opt" value="" id="payty_opt" />

        <br />
        {if $payty_multi_card_mode == 2}
          <p class="tip">{if $payty_avail_cards|@count == 1}{l s='Payment Mean' mod='payty'}{else}{l s='Choose your payment mean' mod='payty'}{/if}</p>

          {assign var=first value=true}
          {foreach from=$payty_avail_cards key="key" item="card"}
            <div class="payty-pm">
              {if $payty_avail_cards|@count == 1}
                <input type="hidden" id="payty_multi_card_type_{$key|escape:'html':'UTF-8'}" name="payty_card_type" value="{$key|escape:'html':'UTF-8'}" >
              {else}
                <input type="radio" id="payty_multi_card_type_{$key|escape:'html':'UTF-8'}" name="payty_card_type" value="{$key|escape:'html':'UTF-8'}" style="vertical-align: middle;"{if $first == true} checked="checked"{/if} >
              {/if}

              <label for="payty_multi_card_type_{$key|escape:'html':'UTF-8'}">
                <img src="{$card['logo']}"
                     alt="{$card['label']|escape:'html':'UTF-8'}"
                     title="{$card['label']|escape:'html':'UTF-8'}">
              </label>

              {assign var=first value=false}
            </div>
          {/foreach}
          <div style="margin-bottom: 12px;"></div>
        {/if}

        <p class="tip">{l s='Choose your payment option' mod='payty'}</p>
        <ul>
          {foreach from=$payty_multi_options key="key" item="option"}
            <li onclick="javascript: $('#payty_opt').val('{$key|escape:'html':'UTF-8'}'); $('#payty_multi').submit();">
              {$option.localized_label|escape:'html':'UTF-8'}
            </li>
          {/foreach}
        </ul>
      </form>
    </a>
  </div>
{/if}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
  </div></div>
{/if}