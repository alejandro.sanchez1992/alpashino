{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
  <div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}

  <div class="payment_module payty payty_choozeo {$payty_tag|escape:'html':'UTF-8'}">
    {if {$payty_choozeo_options|@count} == 1}
      <a href="javascript: $('#payty_choozeo').submit();" title="{l s='Click here to pay with Choozeo' mod='payty'}">
    {else}
      <a class="unclickable" title="{l s='Click on a payment option to pay with Choozeo' mod='payty'}" href="javascript: void(0);">
    {/if}
        <img class="logo" src="{$payty_logo|escape:'html':'UTF-8'}" />{$payty_title|escape:'html':'UTF-8'}

        <form action="{$link->getModuleLink('payty', 'redirect', array(), true)|escape:'html':'UTF-8'}" method="post" id="payty_choozeo">
          <input type="hidden" name="payty_payment_type" value="choozeo" />
          <br />

          {foreach from=$payty_choozeo_options key="key" item="option"}
            <label class="payty_card_click" for="payty_card_type_{$key|escape:'html':'UTF-8'}">
              <input type="radio"
                     name="payty_card_type"
                     id="payty_card_type_{$key|escape:'html':'UTF-8'}"
                     value="{$key|escape:'html':'UTF-8'}" />
              <img src="{$option['logo']}"
                   alt="{$option['label']|escape:'html':'UTF-8'}"
                   title="{$option['label']|escape:'html':'UTF-8'}" />
            </label>
          {/foreach}
        </form>
      </a>
  </div>

  <script type="text/javascript">
    $('div.payment_module.payty_choozeo a img').on('click', function(e) {
      $(this).parent().find('input').prop('checked', true);
      $('#payty_choozeo').submit();
    });
  </script>

  {if {$payty_choozeo_options|@count} == 1}
    <script type="text/javascript">
      $('div.payment_module.payty_choozeo a').on('hover', function(e) {
        $('div.payment_module.payty_choozeo a form .payty_card_click img').toggleClass('hover');
      });
    </script>
  {/if}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
  </div></div>
{/if}