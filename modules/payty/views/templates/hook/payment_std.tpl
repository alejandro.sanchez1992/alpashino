{**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<!-- This meta tag is mandatory to avoid encoding problems caused by \PrestaShop\PrestaShop\Core\Payment\PaymentOptionFormDecorator -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<form action="{$link->getModuleLink('payty', 'redirect', array(), true)|escape:'html':'UTF-8'}"
      method="post"
      id="payty_standard"
      style="margin-left: 2.875rem; margin-top: 1.25rem; margin-bottom: 1rem;{if $payty_is_valid_std_identifier} display: none;{/if}">

  <input type="hidden" name="payty_payment_type" value="standard" />

  {if $payty_is_valid_std_identifier}
    <input id="payty_payment_by_identifier" type="hidden" name="payty_payment_by_identifier" value="1" />
  {/if}

  {if ($payty_std_card_data_mode == 2)}
    {assign var=first value=true}
    {foreach from=$payty_avail_cards key="key" item="card"}
      <div class="payty-pm">
        {if $payty_avail_cards|@count == 1}
          <input type="hidden" id="payty_card_type_{$key|escape:'html':'UTF-8'}" name="payty_card_type" value="{$key|escape:'html':'UTF-8'}" >
        {else}
          <input type="radio" id="payty_card_type_{$key|escape:'html':'UTF-8'}" name="payty_card_type" value="{$key|escape:'html':'UTF-8'}" style="vertical-align: middle;"{if $first == true} checked="checked"{/if} >
        {/if}

        <label for="payty_card_type_{$key|escape:'html':'UTF-8'}">
          <img src="{$card['logo']}"
               alt="{$card['label']|escape:'html':'UTF-8'}"
               title="{$card['label']|escape:'html':'UTF-8'}">
        </label>

        {assign var=first value=false}
      </div>
    {/foreach}
    <div style="margin-bottom: 12px;"></div>

    {if $payty_is_valid_std_identifier}
      <ul>
        {if $payty_std_card_data_mode == 2}
          <li>{l s='You will enter payment data after order confirmation.' mod='payty'}</li>
        {/if}
        <li style="margin: 8px 0px 8px;">
          <span>{l s='OR' mod='payty'}</span>
        </li>
        <li>
          <a href="javascript: void(0);" onclick="paytyOneclickPaymentSelect(1)">{l s='Click here to pay with your registered means of payment.' mod='payty'}</a>
        </li>
      </ul>
    {/if}
  {/if}
</form>