/**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Misc JavaScript functions.
 */

function paytyAddMultiOption(first) {
    if (first) {
        $('#payty_multi_options_btn').hide();
        $('#payty_multi_options_table').show();
    }

    var timestamp = new Date().getTime();

    var rowTpl = $('#payty_multi_row_option').html();
    rowTpl = rowTpl.replace(/PAYTY_MULTI_KEY/g, '' + timestamp);

    $(rowTpl).insertBefore('#payty_multi_option_add');
}

function paytyDeleteMultiOption(key) {
    $('#payty_multi_option_' + key).remove();

    if ($('#payty_multi_options_table tbody tr').length === 1) {
        $('#payty_multi_options_btn').show();
        $('#payty_multi_options_table').hide();
        $('#payty_multi_options_table').append("<input type=\"hidden\" id=\"PAYTY_MULTI_OPTIONS\" name=\"PAYTY_MULTI_OPTIONS\" value=\"\">");
    }
}

function paytyAddOneyOption(first, suffix) {
    if (first) {
        $('#payty_oney' + suffix + '_options_btn').hide();
        $('#payty_oney' + suffix + '_options_table').show();
    }

    var timestamp = new Date().getTime();
    var key = suffix != '' ? /PAYTY_ONEY34_KEY/g : /PAYTY_ONEY_KEY/g;
    var rowTpl = $('#payty_oney' + suffix + '_row_option').html();
    rowTpl = rowTpl.replace(key, '' + timestamp);

    $(rowTpl).insertBefore('#payty_oney' + suffix + '_option_add');
}

function paytyDeleteOneyOption(key, suffix) {
    $('#payty_oney' + suffix + '_option_' + key).remove();

    if ($('#payty_oney' + suffix + '_options_table tbody tr').length === 1) {
        $('#payty_oney' + suffix + '_options_btn').show();
        $('#payty_oney' + suffix + '_options_table').hide();
        $('#payty_oney' + suffix + '_options_table').append("<input type=\"hidden\" id=\"PAYTY_ONEY" + suffix + "_OPTIONS\" name=\"PAYTY_ONEY" + suffix + "_OPTIONS\" value=\"\">");
    }
}

function paytyAddFranfinanceOption(first) {
    if (first) {
        $('#payty_ffin_options_btn').hide();
        $('#payty_ffin_options_table').show();
    }

    var timestamp = new Date().getTime();
    var rowTpl = $('#payty_ffin_row_option').html();
    rowTpl = rowTpl.replace(/PAYTY_FFIN_KEY/g, '' + timestamp);

    $(rowTpl).insertBefore('#payty_ffin_option_add');
}

function paytyDeleteFranfinanceOption(key) {
    $('#payty_ffin_option_' + key).remove();

    if ($('#payty_ffin_options_table tbody tr').length === 1) {
        $('#payty_ffin_options_btn').show();
        $('#payty_ffin_options_table').hide();
        $('#payty_ffin_options_table').append("<input type=\"hidden\" id=\"PAYTY_FFIN_OPTIONS\" name=\"PAYTY_FFIN_OPTIONS\" value=\"\">");
    }
}

function paytyAdditionalOptionsToggle(legend) {
    var fieldset = $(legend).parent();

    $(legend).children('span').toggleClass('ui-icon-triangle-1-e ui-icon-triangle-1-s');
    fieldset.find('section').slideToggle();
}

function paytyCategoryTableVisibility() {
    var category = $('select#PAYTY_COMMON_CATEGORY option:selected').val();

    if (category === 'CUSTOM_MAPPING') {
        $('.payty_category_mapping').show();
        $('.payty_category_mapping select').removeAttr('disabled');
    } else {
        $('.payty_category_mapping').hide();
        $('.payty_category_mapping select').attr('disabled', 'disabled');
    }
}

function paytyDeliveryTypeChanged(key) {
    var type = $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_type').val();

    if (type === 'RECLAIM_IN_SHOP') {
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_address').show();
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_zip').show();
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_city').show();
    } else {
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_address').val('');
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_zip').val('');
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_city').val('');

        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_address').hide();
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_zip').hide();
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_city').hide();
    }

    var speed = $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_speed').val();
    if (speed === 'PRIORITY') {
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_delay').show();
    } else {
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_delay').hide();
    }
}

function paytyDeliverySpeedChanged(key) {
    var speed = $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_speed').val();
    var type = $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_type').val();

    if (speed === 'PRIORITY') {
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_delay').show();
    } else {
        $('#PAYTY_ONEY_SHIP_OPTIONS_' + key + '_delay').hide();
    }
}

function paytyRedirectChanged() {
    var redirect = $('select#PAYTY_REDIRECT_ENABLED option:selected').val();

    if (redirect === 'True') {
        $('#payty_redirect_settings').show();
        $('#payty_redirect_settings select, #payty_redirect_settings input').removeAttr('disabled');
    } else {
        $('#payty_redirect_settings').hide();
        $('#payty_redirect_settings select, #payty_redirect_settings input').attr('disabled', 'disabled');
    }
}

function paytyOneyEnableOptionsChanged() {
    var enable = $('select#PAYTY_ONEY_ENABLE_OPTIONS option:selected').val();

    if (enable === 'True') {
        $('#payty_oney_options_settings').show();
        $('#payty_oney_options_settings select, #payty_oney_options_settings input').removeAttr('disabled');
    } else {
        $('#payty_oney_options_settings').hide();
        $('#payty_oney_options_settings select, #payty_oney_options_settings input').attr('disabled', 'disabled');
    }
}

function paytyFullcbEnableOptionsChanged() {
    var enable = $('select#PAYTY_FULLCB_ENABLE_OPTS option:selected').val();

    if (enable === 'True') {
        $('#payty_fullcb_options_settings').show();
        $('#payty_fullcb_options_settings select, #payty_fullcb_options_settings input').removeAttr('disabled');
    } else {
        $('#payty_fullcb_options_settings').hide();
        $('#payty_fullcb_options_settings select, #payty_fullcb_options_settings input').attr('disabled', 'disabled');
    }
}

function paytyHideOtherLanguage(id, name) {
    $('.translatable-field').hide();
    $('.lang-' + id).css('display', 'inline');

    $('.translation-btn button span').text(name);

    var id_old_language = id_language;
    id_language = id;

    if (id_old_language !== id) {
        changeEmployeeLanguage();
    }
}

function paytyAddOtherPaymentMeansOption(first) {
    if (first) {
        $('#payty_other_payment_means_options_btn').hide();
        $('#payty_other_payment_means_options_table').show();
        $('#PAYTY_OTHER_PAYMENT_MEANS').remove();
    }

    var timestamp = new Date().getTime();

    var rowTpl = $('#payty_other_payment_means_row_option').html();
    rowTpl = rowTpl.replace(/PAYTY_OTHER_PAYMENT_SCRIPT_MEANS_KEY/g, '' + timestamp);

    $(rowTpl).insertBefore('#payty_other_payment_means_option_add');
}

function paytyDeleteOtherPaymentMeansOption(key) {
    $('#payty_other_payment_means_option_' + key).remove();

    if ($('#payty_other_payment_means_options_table tbody tr').length === 1) {
        $('#payty_other_payment_means_options_btn').show();
        $('#payty_other_payment_means_options_table').hide();
        $('#payty_other_payment_means_options_table').append("<input type=\"hidden\" id=\"PAYTY_OTHER_PAYMENT_MEANS\" name=\"PAYTY_OTHER_PAYMENT_MEANS\" value=\"\">");
    }
}

function paytyAddExtraPaymentMeansOption(first) {
    if (first) {
        $('#payty_extra_payment_means_options_btn').hide();
        $('#payty_extra_payment_means_options_table').show();
        $('#PAYTY_EXTRA_PAYMENT_MEANS').remove();
    }

    var timestamp = new Date().getTime();

    var rowTpl = $('#payty_add_payment_means_row_option').html();
    rowTpl = rowTpl.replace(/PAYTY_EXTRA_PAYMENT_MEANS_SCRIPT_KEY/g, '' + timestamp);

    $(rowTpl).insertBefore('#payty_extra_payment_means_option_add');
}

function paytyDeleteExtraPaymentMeansOption(key) {
    $('#payty_extra_payment_means_option_' + key).remove();

    if ($('#payty_extra_payment_means_options_table tbody tr').length === 1) {
        $('#payty_extra_payment_means_options_btn').show();
        $('#payty_extra_payment_means_options_table').hide();
        $('#payty_extra_payment_means_options_table').append("<input type=\"hidden\" id=\"PAYTY_EXTRA_PAYMENT_MEANS\" name=\"PAYTY_EXTRA_PAYMENT_MEANS\" value=\"\">");
    }
}

function paytyCountriesRestrictMenuDisplay(retrictCountriesPaymentId) {
    var countryRestrict = $('#' + retrictCountriesPaymentId).val();
    if (countryRestrict === '2') {
        $('#' + retrictCountriesPaymentId + '_MENU').show();
    } else {
        $('#' + retrictCountriesPaymentId + '_MENU').hide();
    }
}

function paytyOneClickMenuDisplay() {
    var oneClickPayment = $('#PAYTY_STD_1_CLICK_PAYMENT').val();
    if (oneClickPayment == 'True') {
        $('#PAYTY_STD_1_CLICK_MENU').show();
    } else {
        $('#PAYTY_STD_1_CLICK_MENU').hide();
    }
}

function paytyDisplayMultiSelect(selectId) {
    $('#' + selectId).show();
    $('#' + selectId).focus();
    $('#LABEL_' + selectId).hide();
}

function paytyDisplayLabel(selectId, clickMessage) {
    $('#' + selectId).hide();
    $('#LABEL_' + selectId).show();
    $('#LABEL_' + selectId).text(paytyGetLabelText(selectId, clickMessage));
}

function paytyGetLabelText(selectId, clickMessage) {
    var select = document.getElementById(selectId);
    var labelText = '', option;

    for (var i = 0, len = select.options.length; i < len; i++) {
        option = select.options[i];

        if (option.selected) {
            labelText += option.text + ', ';
        }
    }

    labelText = labelText.substring(0, labelText.length - 2);
    if (!labelText) {
        labelText = clickMessage;
    }

    return labelText;
}

function paytySepa1clickPaymentMenuDisplay(sepaMandateModeId) {
    var sepaMandateMode = $('#' + sepaMandateModeId).val();
    if (sepaMandateMode === 'REGISTER_PAY') {
        $('#PAYTY_SEPA_1_CLICK_PAYMNT_MENU').show();
    } else {
        $('#PAYTY_SEPA_1_CLICK_PAYMNT_MENU').hide();
    }
}