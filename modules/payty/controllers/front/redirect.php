<?php
/**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * This controller prepares form and redirects to payment gateway.
 */
class PaytyRedirectModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    private $iframe = false;
    private $logger;

    private $accepted_payment_types = array(
        'standard',
        'multi',
        'oney',
        'oney34',
        'fullcb',
        'franfinance',
        'ancv',
        'sepa',
        'sofort',
        'paypal',
        'choozeo',
        'other',
        'grouped_other'
    );

    public function __construct()
    {
        $this->display_column_left = false;
        $this->display_column_right = version_compare(_PS_VERSION_, '1.6', '<');

        parent::__construct();

        $this->logger = PaytyTools::getLogger();
    }

    public function init()
    {
        $this->iframe = (int) Tools::getValue('content_only', 0) == 1;

        parent::init();
    }

    /**
     * Initializes page header variables
     */
    public function initHeader()
    {
        parent::initHeader();

        // To avoid document expired warning.
        session_cache_limiter('private_no_expire');

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    /**
     * PrestaShop 1.7: override page name in template to use same styles as checkout page.
     * @return array
     */
    public function getTemplateVarPage()
    {
        if (method_exists(get_parent_class($this), 'getTemplateVarPage')) {
            $page = parent::getTemplateVarPage();
            $page['page_name'] = 'checkout';
            return $page;
        }

        return null;
    }

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        $cart = $this->context->cart;

        // Page to redirect to if errors.
        $page = Configuration::get('PS_ORDER_PROCESS_TYPE') ? 'order-opc' : 'order';

        // Check cart errors.
        if (! Validate::isLoadedObject($cart) || $cart->nbProducts() <= 0) {
            $this->paytyRedirect('index.php?controller=' . $page);
        } elseif (! $cart->id_customer || ! $cart->id_address_delivery || ! $cart->id_address_invoice || ! $this->module->active) {
            if (version_compare(_PS_VERSION_, '1.7', '<') && ! Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                $page .= '&step=1'; // Not one page checkout, goto first checkout step.
            }

            $this->paytyRedirect('index.php?controller=' . $page);
        }

        $type = Tools::getValue('payty_payment_type', null); // The selected payment submodule.
        if (! $type && $this->iframe) {
            // Only standard payment can be done inside iframe.
            $type = 'standard';
        }

        if (! in_array($type, $this->accepted_payment_types)) {
            $this->logger->logWarning('Error: payment type "' . $type . '" is not supported. Load standard payment by default.');

            // Do not log sensitive data.
            $sensitive_data = array('payty_card_number', 'payty_cvv', 'payty_expiry_month', 'payty_expiry_year');
            $dataToLog = array();
            foreach ($_REQUEST as $key => $value) {
                if (in_array($key, $sensitive_data)) {
                    $dataToLog[$key] = str_repeat('*', Tools::strlen($value));
                } else {
                    $dataToLog[$key] = $value;
                }
            }

            $this->logger->logWarning('Request data: ' . print_r($dataToLog, true));

            $type = 'standard'; // Force standard payment.
        }

        $payment = null;
        $data = array();

        $option = '';

        switch ($type) {
            case 'standard':
                $payment = new PaytyStandardPayment();

                if ($payment->getEntryMode() === PaytyTools::MODE_LOCAL_TYPE) {
                    $data['card_type'] = Tools::getValue('payty_card_type');
                } elseif ($payment->getEntryMode() === PaytyTools::MODE_IFRAME) {
                    $data['iframe_mode'] = true;
                }

                // Payment by alias.
                if (Configuration::get('PAYTY_STD_1_CLICK_PAYMENT') === 'True') {
                    $data['payment_by_identifier'] = Tools::getValue('payty_payment_by_identifier', '0');
                }

                break;

            case 'multi':
                $data['opt'] = Tools::getValue('payty_opt');
                $data['card_type'] = Tools::getValue('payty_card_type');

                $payment = new PaytyMultiPayment();
                $options = PaytyMultiPayment::getAvailableOptions($cart);
                $option = ' (' . $options[$data['opt']]['count'] . ' x)';
                break;

            case 'oney':
                $payment = new PaytyOneyPayment();

                $data['opt'] = Tools::getValue('payty_oney_option');

                $options = PaytyOneyPayment::getAvailableOptions($cart);
                $option = ' (' . $options[$data['opt']]['count'] . ' x)';
                break;

            case 'oney34':
                $payment = new PaytyOney34Payment();

                $data['opt'] = Tools::getValue('payty_oney34_option');

                $options = PaytyOney34Payment::getAvailableOptions($cart);
                $option = ' (' . $options[$data['opt']]['count'] . ' x)';
                break;

            case 'fullcb':
                $payment = new PaytyFullcbPayment();

                $data['card_type'] = Tools::getValue('payty_card_type');

                $options = PaytyFullcbPayment::getAvailableOptions($cart);
                $option = ' (' . $options[$data['card_type']]['count'] . ' x)';
                break;

            case 'franfinance':
                $payment = new PaytyFranfinancePayment();

                $data['opt'] = Tools::getValue('payty_ffin_option');

                $options = PaytyFranfinancePayment::getAvailableOptions($cart);
                $option = ' (' . $options[$data['opt']]['count'] . ' x)';
                break;

            case 'ancv':
                $payment = new PaytyAncvPayment();
                break;

            case 'sepa':
                $payment = new PaytySepaPayment();

                // Payment by alias.
                if (Configuration::get('PAYTY_SEPA_1_CLICK_PAYMNT') === 'True') {
                    $data['sepa_payment_by_identifier'] = Tools::getValue('payty_sepa_payment_by_identifier', '0');
                }

                break;

            case 'sofort':
                $payment = new PaytySofortPayment();
                break;

            case 'paypal':
                $payment = new PaytyPaypalPayment();
                break;

            case 'choozeo':
                $payment = new PaytyChoozeoPayment();
                $data['card_type'] = Tools::getValue('payty_card_type');
                break;

            case 'other':
                $code = Tools::getValue('payty_payment_code');
                $label = Tools::getValue('payty_payment_title');

                $payment = new PaytyOtherPayment();
                $payment->init($code, $label);

                $data['card_type'] = $code;
                break;

            case 'grouped_other':
                $payment = new PaytyGroupedOtherPayment();

                $data['card_type'] = Tools::getValue('payty_card_type');
                break;
        }

        // Validate payment data.
        $errors = $payment->validate($cart, $data);
        if (! empty($errors)) {
            $this->context->cookie->paytyPayErrors = implode("\n", $errors);

            if (version_compare(_PS_VERSION_, '1.7', '<') && ! Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                $page .= '&step=3';

                if (version_compare(_PS_VERSION_, '1.5.1', '<')) {
                    $page .= '&cgv=1&id_carrier=' . $cart->id_carrier;
                }
            }

            $this->paytyRedirect('index.php?controller=' . $page);
        }

        if (Configuration::get('PAYTY_CART_MANAGEMENT') !== PaytyTools::KEEP_CART) {
            unset($this->context->cookie->id_cart); // To avoid double call to this page.
        }

        // Prepare data for payment gateway form.
        $request = $payment->prepareRequest($cart, $data);
        $fields = $request->getRequestFieldsArray(false, false /* Data escape will be done in redirect template. */);

        $dataToLog = $request->getRequestFieldsArray(true, false);
        $this->logger->logInfo('Data to be sent to payment gateway: ' . print_r($dataToLog, true));

        $this->context->smarty->assign('payty_params', $fields);
        $this->context->smarty->assign('payty_url', $request->get('platform_url'));
        $this->context->smarty->assign('payty_logo', $payment->getLogo());

        // Recover payment method title.
        $title = $payment->getTitle((int) $cart->id_lang) . $option;
        $this->context->smarty->assign('payty_title', $title);

        if ($this->iframe) {
            $this->setTemplate(PaytyTools::getTemplatePath('iframe/redirect.tpl'));
        } else {
            if (version_compare(_PS_VERSION_, '1.7', '>=')) {
                $this->setTemplate('module:payty/views/templates/front/redirect.tpl');
            } else {
                $this->setTemplate('redirect_bc.tpl');
            }
        }
    }

    private function paytyRedirect($url)
    {
        if ($this->iframe) {
            // Iframe mode, use template to redirect to top window.
            $this->context->smarty->assign('payty_url', PaytyTools::getPageLink($url));
            $this->setTemplate(PaytyTools::getTemplatePath('iframe/response.tpl'));
        } else {
            Tools::redirect($url);
        }
    }
}
