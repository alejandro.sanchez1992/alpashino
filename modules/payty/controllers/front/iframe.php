<?php
/**
 * Copyright © Lyra Network.
 * This file is part of Payty plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

class PaytyIframeModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        if (Configuration::get('PAYTY_CART_MANAGEMENT') !== PaytyTools::KEEP_CART) {
            if ($this->context->cart->id) {
                $this->context->cookie->paytyCartId = (int) $this->context->cart->id;
            }

            if (isset($this->context->cookie->paytyCartId)) {
                $this->context->cookie->id_cart = $this->context->cookie->paytyCartId;
            }
        }

        $this->setTemplate(PaytyTools::getTemplatePath('iframe/loader.tpl'));
    }
}
