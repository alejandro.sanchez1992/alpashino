{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

<section class="featured-products clearfix">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h5><span class="brand-primary">/</span> DESTACADOS</h5>
				<p class="products-section-title d-none">
					{l s='Featured' d='Shop.Theme.Catalog'}
				</p>
				{assign var="productscount" value=$products|count}

				<div class="products products-slick spacing-md-top{if $productscount > 1} products--slickmobile{/if}" data-slick='{strip}
					{ldelim}
					"slidesToShow": 1,
					"slidesToScroll": 1,
					"mobileFirst":true,
					"arrows":true,
					"rows":0,
					"responsive": [
					{ldelim}
						"breakpoint": 992,
						"settings":
						{if $productscount > 3}
							{ldelim}
							"arrows":true,
							"slidesToShow": 3,
							"slidesToScroll": 3,
							"arrows":true
							{rdelim}
						{else}
							"unslick"
						{/if}
					{rdelim},
					{ldelim}
						"breakpoint": 720,
						"settings":
						{if $productscount > 3}
							{ldelim}
							"arrows":true,
							"slidesToShow": 3,
							"slidesToScroll": 3
							{rdelim}
						{else}
							"unslick"
						{/if}
					{rdelim},
					{ldelim}
						"breakpoint": 540,
						"settings":
						{if $productscount > 2}
							{ldelim}
							"arrows":true,
							"slidesToShow": 2,
							"slidesToScroll": 2
							{rdelim}
						{else}
							"unslick"
						{/if}
					{rdelim}
					]{rdelim}{/strip}'>
						{foreach from=$products item="product"}
							{include file="catalog/_partials/miniatures/product.tpl" product=$product}
						{/foreach}
					</div>
			</div>
		</div>
	</div>
</section>