{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
        <ul class="top-menu" {if $depth==0}id="top-menu" {/if} data-depth="{$depth}">
            {foreach from=$nodes item=node}
                <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
                    {assign var=_counter value=$_counter+1}
                    <a class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}" href="{$node.url}"
                        data-depth="{$depth}" {if $node.open_in_new_window} target="_blank" {/if}>
                        {if $node.children|count}
                            {* Cannot use page identifier as we can have the same page several times *}
                            {assign var=_expand_id value=10|mt_rand:100000}
                        {/if}
                        {$node.label}
                    </a>
                    {if $node.children|count}
                        <div {if $depth===0} class="popover sub-menu js-sub-menu collapse" {else} class="collapse" {/if}
                            id="top_sub_menu_{$_expand_id}">
                            <div class="container-fluid px-0">
                                <div class="row">
                                    <div class="col-6">
                                        <img src="{$node.image_urls.0}" class="img-fluid" alt="menu img">
                                    </div>
                                    <div class="col-6">
                                        {menu nodes=$node.children depth=$node.depth parent=$node}
                                    </div>
                                </div>
                            </div>

                        </div>
                    {/if}
                </li>
            {/foreach}
        </ul>
    {/if}
{/function}

<div class="menu js-top-menu position-static d-md-block d-none" id="_desktop_top_menu">
    {menu nodes=$menu.children}
</div>


{assign var=_counter value=0}
{function name="menuMobile" nodes=[] depth=0 parent=null}
    {if $nodes|count}
        <div class="nav-mobile d-md-none d-block">
            <ul data-depth="{$depth}">
                <li class="active"><a href="{_PS_BASE_URL_}/{$language.iso_code}/">Inicio </a></li>
                {foreach from=$nodes item=node}
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#{$node.page_identifier}">
                            {$node.label}
                            {if $node.children|count}<span></span>{/if}
                        </a>

                        {if $node.children|count}
                            <ul id="{$node.page_identifier}" class="collapse out submenu">
                                <li><a href="{$node.url}">Ver todo </a></li>
                                {foreach from=$node.children item=node}
                                    <li><a href="{$node.url}">{$node.label} </a></li>
                                {/foreach}
                            </ul>
                        {/if}
                    </li>
                {/foreach}

                <li><a href="{_PS_BASE_URL_}/{$language.iso_code}/contactenos">Contacto </a></li>
            </ul>
        </div><!-- /.nav-mobile -->
    {/if}
{/function}

{menuMobile nodes=$menu.children}