{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

<section class="featured-products clearfix">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11">
                {foreach from=$products item="product" key="position"}
                    <div class="product-item-home d-flex justify-content-center align-items-center">
                        <figure class="mr-4">
                            <img src="{$link->getManufacturerImageLink($product.id_manufacturer, 'small_default')}" alt="{$product.manufacturer_name|escape:html:'UTF-8'}">
                        </figure>

                        <div class="block-img-text">
                            {if $product.cover}
                                <a href="{$product.url}" class="thumbnail product-thumbnail">
                                    <img src="{$product.cover.bySize.large_default.url}"
                                        alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                        loading="lazy" data-full-size-image-url="{$product.cover.large.url}" width="400"
                                        height="400" class="img-fluid" />
                                </a>
                            {else}
                                <a href="{$product.url}" class="thumbnail product-thumbnail">
                                    <img src="{$urls.no_picture_image.bySize.large_default.url}" loading="lazy" width="400"
                                        height="400" class="img-fluid" />
                                </a>
                            {/if}
                            <div class="block-text-item bg-white">
                                <h2 class="title-text"><span class="brand-primary mr-3">/</span><a href="{$product.url}"
                                        content="{$product.url}">{$product.name|truncate:30:'...'}</a></h2>
								<p>{$product.description_short nofilter}</p>
								<a href="{$product.url}" class="btn btn-primary px-5">Comprar</a>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</section>