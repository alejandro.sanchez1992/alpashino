/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */
import 'expose-loader?Tether!tether';
import 'bootstrap/dist/js/bootstrap.min';
import 'flexibility';
import 'bootstrap-touchspin';
import 'jquery-touchswipe';
import './selectors';

import './responsive';
import './checkout';
import './customer';
import './listing';
import './product';
import './cart';

import prestashop from 'prestashop';
import EventEmitter from 'events';
import DropDown from './components/drop-down';
import Form from './components/form';
import ProductMinitature from './components/product-miniature';
import ProductSelect from './components/product-select';
import TopMenu from './components/top-menu';

import './lib/bootstrap-filestyle.min';
import './lib/jquery.scrollbox.min';
import './lib/slick.min';
import SlickSlider from './components/slick';

import './components/block-cart';
import $ from 'jquery';
/* eslint-enable */

// "inherit" EventEmitter
// eslint-disable-next-line
for (const i in EventEmitter.prototype) {
	prestashop[i] = EventEmitter.prototype[i];
}

$(document).ready(() => {
	const dropDownEl = $('.js-dropdown');
	const form = new Form();
	const topMenuEl = $('.js-top-menu ul[data-depth="0"]');
	const dropDown = new DropDown(dropDownEl);
	const topMenu = new TopMenu(topMenuEl);
	const productMinitature = new ProductMinitature();
	const productSelect = new ProductSelect();
	let slickSlider = new SlickSlider();
	dropDown.init();
	form.init();
	slickSlider.init();
	topMenu.init();
	productMinitature.init();
	productSelect.init();

	var stickyOffset1 = $('.sticky-header').offset().top;
	var stickyOffset2 = $('#header').offset().top;

	$(window).scroll(function () {
		if ($(window).width() > 991) {
			var sticky = $('.sticky-header'),
				scroll = $(window).scrollTop();

			if (scroll >= stickyOffset1) {
				$('body').addClass('sticky-pad');
				sticky.addClass('fixed-lg-top');
			} else {
				$('body').removeClass('sticky-pad');
				sticky.removeClass('fixed-lg-top');
			}
		}

		if ($(window).width() < 991) {
			var sticky2 = $('#header'),
				scroll2 = $(window).scrollTop();

			if (scroll2 >= stickyOffset2) {
				$('body').addClass('sticky-pad');
				sticky2.addClass('fixed-sm-top');
			} else {
				$('body').removeClass('sticky-pad');
				sticky2.removeClass('fixed-sm-top');
			}
		}
	});

	$('.menu-mobile').on("click", function () {
		if (!$("nav.sidebar").hasClass("active")) {
			displayModal(true);
			$("body").addClass("open-modal");
			$("nav.sidebar").addClass("active");
			$(".overlay").addClass("active");
		} else {
			displayModal(false);
			$("body").removeClass("open-modal");
			$("nav.sidebar").removeClass("active");
			$(".overlay").removeClass("active");
		}
	});

	$(".overlay, .dismiss").on("click", function () {
		if ($(".sidebar").hasClass("active") || $(".mobile-filters").hasClass("active")) {
			displayModal(false);
			$("body").removeClass("open-modal");
			$(".mobile-filters").removeClass("active");
			$("nav.sidebar").removeClass("active");
			$(".overlay").removeClass("active");
		}
	});

	var documentBody = $('body');

	function displayModal(display) {
		if (display) {
			if (isiPhone()) {
				documentBody.css('position', 'fixed');
			} else {
				documentBody.css('overflow', 'hidden');
			}
		} else {
			documentBody.removeAttr('style');
		}
	}

	function isiPhone() {
		return (
			(navigator.platform.indexOf("iPhone") != -1) ||
			(navigator.platform.indexOf("iPod") != -1)
		);
	}

	$('.carousel[data-touch="true"]').swipe({
		swipe(event, direction, distance, duration, fingerCount, fingerData) {
			if (direction == 'left') {
				$(this).carousel('next');
			}
			if (direction == 'right') {
				$(this).carousel('prev');
			}
		},
		allowPageScroll: 'vertical',
	});
});
