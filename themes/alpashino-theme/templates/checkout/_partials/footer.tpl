{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div class="modal fade js-checkout-modal" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme.Global'}">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="js-modal-content"></div>
        </div>
    </div>
</div>

<div class="footer-container">
    <div class="container">
        <div class="row">
            {block name='hook_footer'}
                {hook h='displayFooter'}
            {/block}
        </div>
        <div class="row">
            <div class="col-12">
                <div class="sub-footer-logo">
                    <a href="#" class="p-4 mb-2 d-inline-block top-up"><i class="fas fa-chevron-up"></i></a>
                    <ul>
                        <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                    <hr class="line">
                    <a href="{$urls.base_url}"><img src="{$urls.img_url}global/footer-logo-alphasino.png" width="150"
                            class="img-fluid my-4 mx-auto" alt="logo" /></a>
                </div>
            </div>
        </div>
    </div>
    <p class="text-center mb-0 p-1 copyright-link">
        {block name='copyright_link'}
            <a class="_blank" href="{$urls.base_url}" target="_blank" rel="nofollow">
                {l s='%copyright% %year% - Todos los derechos reservados / Alpashino / Resalto Agencia' sprintf=['%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme.Global'}
            </a>
        {/block}
    </p>
</div>