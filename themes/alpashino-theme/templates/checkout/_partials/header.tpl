{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 {block name='header_top_black'}
    <div class="header-top-black d-none d-lg-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <span class="country"><img src="{$urls.img_url}global/colombia.svg" width="15" class="mr-2"
                            alt="icon-col"> Envíos gratis a todo Colombia</span>
                </div>
                <div class="col-2">
                    {if $customer.is_logged}
                        <a href="{$urls.pages.my_account}" class="register"><i class="las la-angle-right"></i> Mi cuenta</a>
                    {else}
                        <a href="{$urls.pages.register}" class="register"><i class="las la-angle-right"></i> Regístrate</a>
                    {/if}
                </div>
                <div class="col-2 pr-0">
                    <a href="" class="btn-wsp"><i class="fab fa-whatsapp"></i> 300405300</a>
                </div>
            </div>
        </div>
    </div>

    <div class="header-top-black header-mobile d-block d-lg-none py-1">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-2">
                    <span class="country"><img src="{$urls.img_url}global/colombia.svg" width="20" class="mr-2"
                            alt="icon-col"></span>
                </div>
                <div class="col-8 px-0">
                    <div class="search-block">
                        {hook h='displaySearch'}
                    </div>
                </div>
                <div class="col-2">
                    <ul class="icons-header m-0">
                        {if $customer.is_logged}
                            <li><a href="{$urls.pages.my_account}" class="register"><i class="fas fa-user"></i></a></li>
                        {else}
                            <li><a href="{$urls.pages.register}" class="register"><i class="fas fa-user"></i></a></li>
                        {/if}
                    </ul>
                </div>
            </div>
        </div>
        <div class="overlay"></div><!-- /.overlay -->
    </div>
{/block}

{block name='header_banner'}
    <div class="header-banner">
        {hook h='displayBanner'}
    </div>
{/block}

<div class="sticky-header bg-white">
    {block name='header_nav'}
        <!-- Sidebar -->
        <nav class="sidebar">
            <div class="nav-head row m-0 align-items-center">
                <div class="dismiss">
                    <i class="fas fa-times"></i>
                </div>

                <div class="sidebar-header col-8">
                    <a href="{$urls.base_url}" class="d-block">
                        <img class="logo img-fluid" src="{$shop.logo}" alt="{$shop.name}">
                    </a>
                </div>
            </div><!-- /.nav-head -->

            <div class="mobile-menu">
                {widget name='ps_mainmenu'}
            </div><!-- /.mobile-menu -->
        </nav>

        <div class="header-nav">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-3 d-block d-lg-none">
                        <a href="#no" class="menu-mobile d-block d-md-none"><i class="fas fa-bars"></i></a>
                    </div>
                    <div class="col-lg-3 col-6 px-0 px-lg-2">
                        {block name='header_logo'}
                            <a class="logo d-block text-center" href="{$urls.base_url}" title="{$shop.name}">
                                <img src="{$shop.logo}" class="img-fluid" alt="{$shop.name}">
                            </a>
                        {/block}
                    </div>
                    <div class="col-lg-6 d-lg-block d-none">
                        <div class="search-block px-lg-5">
                            {hook h='displaySearch'}
                        </div>
                    </div>
                    <div class="col-lg-3 col-3">
                        {widget name='ps_shoppingcart'}
                    </div>
                </div>
            </div>
        </div>
    {/block}

    {block name='header_top'}
        <div class="header-top d-md-flex d-none">
            {hook h='displayTop'}
        </div>

        {hook h='displayNavFullWidth'}

    {/block}
</div>