<?php
use PrestaShop\PrestaShop\Adapter\ServiceLocator;
use PrestaShop\PrestaShop\Adapter\CoreException;
class Customer extends CustomerCore
{
	/*
    * module: ps_customerdocumenttype
    * date: 2021-04-28 15:49:55
    * version: 1.0.1
    */
	
	/*
    * module: ps_customerdocumenttype
    * date: 2021-04-28 15:49:55
    * version: 1.0.1
    */
	
	/*
    * module: ps_customerdocumenttype
    * date: 2021-04-28 15:49:55
    * version: 1.0.1
    */
	
	/*
    * module: ps_customerdocumenttype
    * date: 2021-04-28 15:49:55
    * version: 1.0.1
    */
	
    /*
    * module: ps_customerdocumentnumber
    * date: 2021-05-28 22:09:17
    * version: 2.0.0
    */
    public $document_number;
    /*
    * module: ps_customerdocumentnumber
    * date: 2021-05-28 22:09:17
    * version: 2.0.0
    */
    public function __construct($id = null)
    {
        self::$definition['fields']['document_number'] = array('type' => self::TYPE_STRING, 'required' => true,  'validate' => 'isGenericName');
        parent::__construct($id);
    }
}
